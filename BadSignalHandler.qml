import QtQuick

QtObject {
    id: root
    
    signal pickingReplyReceived(var replyData)
    signal doublePickingReplyReceived(var firstReplyData, var secondReplyData)
}
