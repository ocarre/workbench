import QtQuick
import QtQuick.Controls
import QtQuick.Window
import QtQml.StateMachine

State {
    id: root

    property bool ready: false;

    property var initializer: () => {} /// \return promise or nothing
    property var deinitializer: () => {} /// \return promise or nothing

    onEntered: () => { console.log("+OnState", root.objectName); Promise.resolve(initializer()).then(() => ready = true); }
    onExited: () => { console.log("-OnState", root.objectName); }

    function canGoto(nextState) {
        return !root.transitions.every(x => !(nextState === x.targetState && x.guard));
    }

    function goto(nextState) {
        ready = false;

        Promise.resolve(deinitializer()).then(() => {
            var acceptableTransitions = !root.transitions.filter(x => nextState === x.targetState && x.guard);
            if(0 === acceptableTransitions.length) {
                console.error(`No available transition to go to state ${nextState.objectName}`);
                return;
            }

            var selectedTransition = acceptableTransitions[0];
            selectedTransition.signal();
        }).catch(() => ready = true);
    }
}
