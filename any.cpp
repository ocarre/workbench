#include <stdexcept>
#include <iostream>

void bar()
{
    try
    {
        throw std::runtime_error("first");
    }
    catch(...)
    {
        std::throw_with_nested(std::runtime_error("second"));
    }
}

void foo()
{
    try
    {
        bar();
    }
    catch(...)
    {
        // should silent exception BUT leads to "trace trap"
        std::cout << "silent" << std::endl;
    }
}

int main()
{
    std::cout << "start" << std::endl;

    foo();

    std::cout << "end" << std::endl;

    return 0;
}
