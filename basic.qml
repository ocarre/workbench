import QtQml
import QtQuick 2.15
import QtQuick.Window 2.15

Window {
    width: 200
    height: 200
    visible: true

    property list<int> test: [1, 2, 3]

    Rectangle {
        anchors.fill: parent

        color: "red"

        Component.onCompleted: {
            var func = () => console.log("hello");
            func();

            let e = new Error("profiling");
            console.log(e.fileName, e.lineNumber, e.message);
        }
    }
}
