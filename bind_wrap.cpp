#include <iostream>
#include <functional>
#include <list>

class Pipeline
{
public:
    class Lock
    {
    public:
        Lock(Pipeline& pipeline) :
            myPipeline(pipeline)
        {

        }

        Lock(const Lock&) = delete;
        Lock& operator=(const Lock&) = delete;
    
        ~Lock()
        {
            myPipeline.unlock();
        }

    private:
        Pipeline& myPipeline;

    };

    class Stage
    {
    public:
        Stage(Pipeline& pipeline) :
            myPipeline(pipeline)
        {

        }

        void operator()()
        {
            myPipeline.next();
        }

    private:
        Pipeline& myPipeline;

    };

    using Continuation = std::function<void(Pipeline::Stage&)>;

public:
    Pipeline() :
        myContinuations(),
        myIterator(myContinuations.end()),
        myRunning(false)
    {
        
    }
        
    void execute()
    {
        if(myRunning)
            throw std::runtime_error("Pipeline already running");

        myRunning = true;
        myIterator = myContinuations.begin();

        next();
        
        myRunning = false;
    }
    
    template<class F>
    Lock lock(F function)
    {
        if(myRunning)
            throw std::runtime_error("Cannot add a continuation during pipeline execution");

        myContinuations.push_back(function);

        return Lock(*this);
    }

protected:
    void unlock()
    {
        myContinuations.pop_back();
    }

    void next()
    {
        if(!myRunning)
            throw std::runtime_error("Pipeline is not running");
            
        const std::list<Continuation>::const_iterator current_iterator = myIterator++;

        if(myContinuations.end() != current_iterator)
        {
            Stage stage(*this);
            (*current_iterator)(stage);
            
            std::list<Continuation>::const_iterator next_iterator = current_iterator;
            next_iterator++;

            if(next_iterator == myIterator)
            {
                throw std::runtime_error("Pipeline::Stage call is missing");
            }
        }
    }

private:
    std::list<Continuation> myContinuations;
    std::list<Continuation>::const_iterator myIterator;
    bool myRunning;
    
};

static int count = 0;

static void a(Pipeline::Stage& stage)
{
    std::cout << "+A" << " " << count++ << std::endl;
    
    stage();
    
    std::cout << "-A" << " " << --count << std::endl;
}

static void b(Pipeline::Stage& stage)
{
    std::cout << "+B" << " " << count++ << std::endl;
    
    stage();
    
    std::cout << "-B" << " " << --count << std::endl;
}

static void c(Pipeline::Stage& stage)
{
    std::cout << "+C" << " " << count++ << std::endl;
    
    stage();
    
    std::cout << "-C" << " " << --count << std::endl;
}

static void d(Pipeline::Stage& stage)
{
    std::cout << "+D" << " " << count << std::endl;

    stage();

    std::cout << "-D" << " " << count << std::endl;
}

int main() try
{
    std::cout << "+++++" << std::endl;
    
    Pipeline pipeline;
    
    const auto la = pipeline.lock(&a);
    const auto lb = pipeline.lock(&b);
    const auto lc = pipeline.lock(&c);
    
    {
        const auto ld = pipeline.lock(&d);
        
        pipeline.execute();
    }

    std::cout << "=====" << std::endl;

    pipeline.execute();
    
    std::cout << "-----" << std::endl;

    return 0;
}
catch(const std::exception& e)
{
    std::cerr << e.what() << std::endl;
}
