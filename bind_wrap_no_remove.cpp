#include <iostream>
#include <functional>
#include <list>

class Pipeline
{
public:
    class Stage
    {
    public:
        Stage(Pipeline& pipeline) :
            myPipeline(pipeline)
        {

        }

        void operator()()
        {
            myPipeline.next();
        }

    private:
        Pipeline& myPipeline;

    };

    using Continuation = std::function<void(Pipeline::Stage&)>;
    using Terminal = std::function<void()>;

public:
    Pipeline() :
        myContinuations(),
        myIterator(myContinuations.end()),
        myTerminal()
    {
        
    }

    template<class F>
    void add(F function)
    {
        if(running())
            throw std::runtime_error("Cannot add a continuation during pipeline execution");

        myContinuations.push_back(function);
    }

    void execute(const Terminal& terminal)
    {
        if(running())
            throw std::runtime_error("Pipeline already running");

        myTerminal = terminal;

        myIterator = myContinuations.begin();

        next();

        myTerminal = {};
    }

protected:
    bool running() const { return !!myTerminal; }

    void next()
    {
        if(!running())
            throw std::runtime_error("Pipeline is not running");
            
        const std::list<Continuation>::const_iterator current_iterator = myIterator++;

        if(myContinuations.end() != current_iterator)
        {
            Stage stage(*this);
            (*current_iterator)(stage);
            
            std::list<Continuation>::const_iterator next_iterator = current_iterator;
            next_iterator++;

            if(next_iterator == myIterator)
            {
                throw std::runtime_error("Pipeline::Stage call is missing");
            }
        }
        else
        {
            myTerminal();
        }
    }

private:
    std::list<Continuation> myContinuations;
    std::list<Continuation>::const_iterator myIterator;
    Terminal myTerminal;
    
};

static int count = 0;

static void a(Pipeline::Stage& next)
{
    std::cout << "+A" << " " << count++ << std::endl;
    
    next();
    
    std::cout << "-A" << " " << --count << std::endl;
}

static void b(Pipeline::Stage& next)
{
    std::cout << "+B" << " " << count++ << std::endl;
    
    next();
    
    std::cout << "-B" << " " << --count << std::endl;
}

static void c(Pipeline::Stage& next)
{
    std::cout << "+C" << " " << count++ << std::endl;
    
    next();
    
    std::cout << "-C" << " " << --count << std::endl;
}

static void d()
{
    std::cout << "=D" << " " << count << std::endl;
}

int main() try
{
    std::cout << "+++++" << std::endl;
    
    Pipeline pipeline;
    
    pipeline.add(&a);
    pipeline.add(&b);
    pipeline.add(&c);
    
    pipeline.execute(&d);

    std::cout << "=====" << std::endl;

    pipeline.execute(&d);
    
    std::cout << "-----" << std::endl;

    return 0;
}
catch(const std::exception& e)
{
    std::cerr << e.what() << std::endl;
}
