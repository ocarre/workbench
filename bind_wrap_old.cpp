#include <iostream>
#include <functional>
#include <list>

class Pipeline
{
    using Continuation = std::function<void(Pipeline&)>;
    using Terminal = std::function<void()>;
    
public:
    Pipeline() :
        continuations(),
        iterator(continuations.end()),
        terminal(),
        running(false)
    {
        
    }
    
    void operator()()
    {
        if(!running)
            throw std::runtime_error("Binder is not running");
            
        const std::list<Continuation>::const_iterator current_iterator = iterator++;
        
        if(continuations.end() == current_iterator)
        {
            terminal();
        }
        else
        {
            (*current_iterator)(*this);

            std::list<Continuation>::const_iterator next_iterator = current_iterator;
            next_iterator++;

            if(next_iterator == iterator)
            {
                throw std::runtime_error("Binder did not call continuation");
            }
        }
    }
    
    void operator()(Terminal term)
    {
        terminal = term;
        iterator = continuations.begin();
        running = true;
        
        operator()();
        
        running = false;
        terminal = {};
    }
    
    template<class F>
    void append(F function)
    {
        if(running)
            throw std::runtime_error("Cannot add a continuation during binder execution");

        continuations.push_back(function);
    }

private:
    std::list<Continuation> continuations;
    std::list<Continuation>::const_iterator iterator;
    Terminal terminal;
    bool running;
    
};

static int count = 0;

static void a(Pipeline& binder)
{
    std::cout << "+A" << " " << count++ << std::endl;
    
    binder();
    
    std::cout << "-A" << " " << --count << std::endl;
}

static void b(Pipeline& binder)
{
    std::cout << "+B" << " " << count++ << std::endl;
    
    binder();
    
    std::cout << "-B" << " " << --count << std::endl;
}

static void c(Pipeline& binder)
{
    std::cout << "+C" << " " << count++ << std::endl;
    
    binder();
    
    std::cout << "-C" << " " << --count << std::endl;
}

static void d()
{
    std::cout << "=D" << " " << count << std::endl;
}

int main() try
{
    std::cout << "+++++" << std::endl;
    
    Pipeline binder;
    
    binder.append(&a);
    binder.append(&b);
    binder.append(&c);
    
    binder(&d);

    std::cout << "=====" << std::endl;

    binder(&d);
    
    std::cout << "-----" << std::endl;

    return 0;
}
catch(const std::exception& e)
{
    std::cerr << e.what() << std::endl;
}
