import QtQuick
import QtQml.Models

Column {
    id: root
    
    component Test: ObjectModel {
        id: objectModel

        property color color: "white"

        Rectangle {
            width: 200
            height: 50
            color: objectModel.color

            Component.onCompleted: console.log("ok");
        }
    }

    // property list<Test> model: [Test { color: "red" }]
    // property list<Rectangle> model: [Rectangle { width: 200; height: 50; color: "red"; Component.onCompleted: console.log("meh"); }]
    
    property list<Test> model: [
        Test { color: "red" },
        Test { color: "green" },
        Test { color: "blue" }
    ]

    Repeater {
        model: root.model
        // model: Test { color: "red" }
    }
}
