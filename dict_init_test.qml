import QtQml
import QtQuick 2.9

Item {
	readonly property var key_1: "key_1"
	readonly property var key_2: "key_2"
	readonly property var key_3: "key_3"
	readonly property var dict0: ({key_1: "sub_key_1", key_2: "sub_key_2", key_3: "sub_key_3"})

	readonly property var values: [1, 2, 3]
	readonly property var dict1: ({[dict0.key_1]: values[0], [dict0.key_2]: values[1], [dict0.key_3]: values[2]})

	Component.onCompleted: {
		for(const [k0, v0] of Object.entries(dict0))
	  		console.log(`Dict0 -> ${k0}: ${v0}`);

	  	for(const [k1, v1] of Object.entries(dict1))
	  		console.log(`Dict1 -> ${k1}: ${v1}`);
	}
}
