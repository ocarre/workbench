#pragma once

#include <cstddef>

using ComponentIndex = std::size_t;
constexpr static ComponentIndex InvalidComponentIndex = -1;
