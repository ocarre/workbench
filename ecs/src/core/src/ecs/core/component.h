#pragma once

#include "container.h"
#include "entity.h"

class Entity;

template<class T>
class Entry
{
public:
    Entry(Container<T>* container);
    ~Entry();

    const T& read() const;
    void write(const T& data);

private:
    Container<T>* myContainer;
    ComponentIndex myComponentIndex;

};

template<class T>
inline Entry<T>::Entry(Container<T>* container) :
    myContainer(container),
    myComponentIndex(InvalidComponentIndex)
{
    myComponentIndex = myContainer->store();
}

template<class T>
inline Entry<T>::~Entry()
{
    myContainer->unstore(myComponentIndex);
}

template<class T>
class Component
{
public:
    Component(Entity* entity);

    void write(const T& datum);
    const T& read() const;

private:
    Entity* myEntity;
    Entry<T> myEntry;

};

template<class T>
inline const T& Entry<T>::read() const
{
    return static_cast<Container<T>*>(myContainer)->read(myComponentIndex);
}

template<class T>
inline void Entry<T>::write(const T& data)
{
    static_cast<Container<T>*>(myContainer)->write(myComponentIndex, data);
}

template<class T>
inline Component<T>::Component(Entity* entity) :
    myEntity(entity),
    myEntry(entity->graph()->container<T>)
{
    
}

template<class T>
inline void Component<T>::write(const T& datum)
{
    myEntry->write(datum);
}

template<class T>
inline const T& Component<T>::read() const
{
    return myEntry->read();
}
