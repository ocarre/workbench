#include "transform.h"

Transform::Transform(Entity* entity, const TransformData& data) : Component(entity)
{
    write<Transform>(data);
}

const Vector3& Transform::position() const
{
    return read<Transform>().position;
}
