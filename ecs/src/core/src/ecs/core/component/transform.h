#pragma once

#include "../component.h"
#include "../vector3.h"

struct TransformData
{
    Vector3 position;
};

class Transform : public Component<TransformData>
{
public:
    Transform(Entity* entity, const TransformData& data);

    const Vector3& position() const;

};
