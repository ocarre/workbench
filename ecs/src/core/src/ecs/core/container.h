#pragma once

#include "basecomponent.h"

#include <vector>

class BaseContainer
{
public:
    // template<class T>
    // const T::Data& read();

    // template<class T>
    // void write(const typename T::Data& data);

};

template<class T>
class Container : public BaseContainer
{
public:
    Container();

    const T::Data& read(ComponentIndex componentIndex) const;
    void write(ComponentIndex componentIndex, const typename T::Data& datum);

private:
    std::vector<typename T::Data> myData;

};

// template<class T>
// const T::Data& BaseContainer::read()
// {
//     return static_cast<Container<T>*>(this)->read();
// }

// template<class T>
// void BaseContainer::write(const typename T::Data& data)
// {
//     static_cast<Container<T>*>(this)->write(data);
// }

template<class T>
inline Container<T>::Container() : BaseContainer(),
    myData()
{

}

template<class T>
const T::Data& Container<T>::read(ComponentIndex componentIndex) const
{
    return myData[componentIndex];
}

template<class T>
void Container<T>::write(ComponentIndex componentIndex, const typename T::Data& datum)
{
    myData[componentIndex] = datum;
}
