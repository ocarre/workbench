#include "entity.h"

#include "component.h"
#include "graph.h"

Entity::Entity(Graph* graph) :
    myGraph(graph),
    myParent(nullptr),
    myName("Root"),
    myEntities(),
    myComponents()
{

}

Entity::Entity(Entity* parent, const std::string_view& name) :
    myGraph(parent->graph()),
    myParent(parent),
    myName(name),
    myEntities(),
    myComponents()
{

}

Entity* Entity::createChild(const std::string_view& name)
{
    Entity* entity = new Entity(this, name);

    myEntities.push_back(entity);

    return entity;
}
