#pragma once

#include "system.h"
#include "component.h"
#include "entity_p.h"
#include "graph_p.h"

#include <unordered_map>
#include <typeindex>
#include <vector>
#include <string>

template<class T>
inline std::optional<ComponentIndex> Entity::getComponentIndex()
{
    const auto iterator = myComponents.find(typeid(T));

    if(myComponents.end() == iterator)
        return {};

    return iterator->second;
}

template<class T, class ... Args>
inline T Entity::createComponent(Args&&... args)
{   
    // ComponentIndex component = system->create(std::forward<Args>(args)...);
    ComponentIndex component = myGraph->createComponent(std::forward<Args>(args)...);

    myComponents.emplace(typeid(T), component);

    return T(system, component);
}
