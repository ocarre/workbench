#pragma once


#include "component.h"

#include <unordered_map>
#include <typeindex>
#include <string>

class Graph;

class Entity
{
public:
    Entity(Graph* graph);

private:
    Entity(Entity* parent, const std::string_view& name);

public:
    const std::string& name() const { return myName; }

    Graph* graph() const { return myGraph; }
    Entity* parent() const { return myParent; }

    const std::vector<Entity*> children() const { return myEntities; }

    Entity* createChild(const std::string_view& name);

    template<class T>
    std::optional<ComponentIndex> getComponentIndex();

    template<class T, class ... Args>
    T createComponent(Args&&... args);

private:
    Graph* myGraph;
    Entity* myParent;
    std::string myName;
    std::vector<Entity*> myEntities;
    std::unordered_map<std::type_index, ComponentIndex> myComponents;

};
