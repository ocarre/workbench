#pragma once

#include "container.h"
#include "entity.h"
#include "component.h"
#include "graph_p.h"
#include "entity_p.h"

#include <stack>
#include <vector>
#include <unordered_map>
#include <string>
#include <typeindex>
#include <iostream>

template<class T> Container<T>* Graph::container()
{
    const auto iterator: myContainers.find(typeid(T));

    if(myContainers.end() == iterator)
    {
        Container<T>* container = new Container<T>(this);

        myContainers.emplace(typeid(T), container);

        return container;
    }

    return static_cast<Container<T>*>(iterator->second);
}

template<class T>
inline System<T>* Graph::createSystem()
{
    const auto iterator = mySystems.find(typeid(T));
    if(mySystems.end() != iterator)
        throw std::runtime_error("System already exists");

    System<T>* system = new System<T>(this);

    mySystems.emplace(typeid(T), system);

    return system;
}

template<class T>
inline System<T>* Graph::getSystem() const
{
    auto iterator = mySystems.find(typeid(T));
    if(mySystems.end() == iterator)
        throw std::runtime_error("System(" + std::string(typeid(T).name()) + ") does not exist");

    return static_cast<System<T>*>(iterator->second);
}

template<class T>
inline std::vector<ComponentIndex> Graph::hierarchy() const
{
    std::cout << "<Hierarchy>" << std::endl;

    std::vector<ComponentIndex> hierarchy;
    hierarchy.resize(getSystem<T>()->size());

    std::stack<std::tuple<std::optional<ComponentIndex>, Entity*>> visitor;
    visitor.push({ {}, myRoot });

    while(!visitor.empty())
    {
        auto [ancestorComponent, entity] = visitor.top();
        visitor.pop();

        const std::optional<ComponentIndex> parentComponent = entity->getComponentIndex<T>();
        if(parentComponent.has_value())
            ancestorComponent = parentComponent;

        for(Entity* child : entity->children())
        {
            std::optional<ComponentIndex> childComponent = child->getComponentIndex<T>();
            if(childComponent.has_value())
            {
                hierarchy[childComponent.value()] = ancestorComponent.has_value() ? ancestorComponent.value() : childComponent.value();

                std::cout << child->name() << " - " << (ancestorComponent.has_value() ? std::to_string(ancestorComponent.value()) : "ø") << " -> " << (childComponent.has_value() ? std::to_string(childComponent.value()) : "ø") << std::endl;
            }

            visitor.push({ ancestorComponent, child });
        }
    }

    std::cout << "</Hierarchy>" << std::endl;

    return hierarchy;
}
