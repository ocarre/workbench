#pragma once

#include "component.h"

#include <vector>
#include <unordered_map>
#include <typeindex>

class BaseSystem;
template<class T> class System;

class BaseContainer;
template<class T> class Container;

class Entity;

class Graph
{
    friend class Entity;

public:
    Graph();

public:
    Entity* root() const { return myRoot; }

    template<class T> Container<T>* container();

    template<class T> System<T>* createSystem();
    template<class T> System<T>* getSystem() const;

    /// \brief give the parent component for every instance of T, or itself in case of root
    /// \todo cache to avoid repetitive graph traversal
    template<class T> std::vector<ComponentIndex> hierarchy() const;
    
private:
    Entity* myRoot;
    std::unordered_map<std::type_index, BaseContainer*> myContainers;
    std::unordered_map<std::type_index, BaseSystem*> mySystems;

};
