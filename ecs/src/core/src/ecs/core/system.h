#pragma once

// #include <vector>
// #include <stack>
// #include <unordered_map>
// #include <optional>
// #include <typeindex>
// #include <typeinfo>
// #include <sstream>

class BaseSystem
{
public:
    virtual ~BaseSystem() = default;

};

template<class T>
class System;
