#include "transform.h"
#include "../component/transform.h"

void System<Transform>::update()
{
    myWorldPositions.resize(myPositions.size());
    std::copy(myPositions.begin(), myPositions.end(), myWorldPositions.begin());

    const std::vector<ComponentIndex>& hierarchy = myGraph->hierarchy<Transform>();
    for(ComponentIndex i = 0; i < myWorldPositions.size(); ++i)
    {
        ComponentIndex parentIndex = hierarchy[i];
        if(parentIndex == i)
            continue;

        myWorldPositions[i] += myWorldPositions[parentIndex];
    }

    std::cout << "WorldPositions : " << myWorldPositions << std::endl;
}

System<Transform>::System(Graph* graph) :
    myGraph(graph)
{
    
}

ComponentIndex System<Transform>::create(const Vector3& position)
{
    myPositions.push_back(position);
    myWorldPositions.push_back(position);

    ComponentIndex index = myPositions.size() - 1;

    return index;
}
