#pragma once

#include "../graph.h"
#include "../system.h"
#include "../vector3.h"
#include "../component/transform.h"

#include <vector>

class Transform;

template<>
class System<Transform> : public BaseSystem
{
    friend class Graph;
    friend class Entity;
    friend class Transform;

public:
    void update();

protected:
    System(Graph* graph);

    ComponentIndex create(const Vector3& position);

    const Vector3& position(const ComponentIndex index) const { return myPositions[index]; }
    const Vector3& worldPosition(const ComponentIndex index) const { return myWorldPositions[index]; }
    
    std::size_t size() const { return myPositions.size(); }

private:
    Graph* myGraph;

private: // inputs
    std::vector<Vector3> myPositions;

private: // outputs
    std::vector<Vector3> myWorldPositions;

};
