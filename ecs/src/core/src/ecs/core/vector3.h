#pragma once

#include <sstream>

struct Vector3
{
    float x;
    float y;
    float z;
};

inline Vector3 operator+(const Vector3& u, const Vector3& v)
{
    return { u.x + v.x, u.y + v.y, u.z + v.z };
}

inline Vector3& operator+=(Vector3& u, const Vector3& v)
{
    u.x += v.x;
    u.y += v.y;
    u.z += v.z;

    return u;
}

inline std::ostream& operator<<(std::ostream& s, const Vector3& u)
{
    s << "Vector3(" << u.x << ", " << u.y << ", " << u.z << ")";

    return s;
}

template<class T>
inline std::ostream& operator<<(std::ostream& s, const std::vector<T>& u)
{
    s << "Vector(";

    bool first = true;
    for(auto value : u)
    {
        if(!first)
        {
            s << ", ";
        }
        else
        {
            first = false;
        }

        s << value;
    }

    s << ")";

    return s;
}
