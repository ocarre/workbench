#include <ecs/core/vector3.h>
#include <ecs/core/graph.h>
#include <ecs/core/entity.h>
#include <ecs/core/system.h>
#include <ecs/core/component/transform.h>
#include <ecs/core/system/transform.h>

#include <vector>
#include <stack>
#include <unordered_map>
#include <optional>
#include <typeindex>
#include <typeinfo>
#include <sstream>
#include <iostream>

// class Render
// {

// };

// template<>
// class System<Render> : public BaseSystem
// {
//     void update()
//     {
    
//     }

// public:

// private:

// };

int main(int, char**)
{
    Graph graph;

    // System<Render>* renderSystem = graph.createSystem<Render>();
    
    Entity* entity = graph.root();
    {
        Entity* entityA = entity->createChild("A");
        entityA->createComponent<Transform>(Vector3({ 1, 0, 0 }));
        {
            Entity* entityAA = entityA->createChild("AA");
            entityAA->createComponent<Transform>(Vector3({ 0, 1, 0 }));

            Entity* entityAB = entityA->createChild("AB");
            entityAB->createComponent<Transform>(Vector3({ 0, 0, 1 }));
            {
                Entity* entityABA = entityAB->createChild("ABA");
                entityABA->createComponent<Transform>(Vector3({ 0, 0, 8 }));

                Entity* entityABB = entityAB->createChild("ABB");
                Transform transformABB = entityABB->createComponent<Transform>(Vector3({ 0, 0, 16 }));

                std::cout << "transformABB::position : " << transformABB.position() << std::endl;
            }
            Entity* entityAC = entityA->createChild("AC");
            {
                Entity* entityACA = entityAC->createChild("ACA");
                (void) entityACA;

                Entity* entityACB = entityAC->createChild("ACB");
                (void) entityACB;
            }
        }
    }

    renderSystem->update();

    return 0;
}
