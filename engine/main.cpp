#include <functional>
#include <iostream>
#include <memory>
#include <vector>
#include <map>

/// \brief uid corresponding to a specific command
using CommandId = int;

/// \brief registry assigning an uid to every kind of command
class CommandRegistry
{
    template <class T>
    class Entry
    {
    public:
        static CommandId Id()
        {
            if(-1 == OurId)
                OurId = Generate();

            return OurId;
        }

    private:
        static CommandId OurId;

    };

public:
    template<class T>
    static CommandId Id()
    {
        return Entry<T>::Id();
    }

    static CommandId Count()
    {
        return OurIdGenerator;
    }

protected:
    static CommandId Generate()
    {
        return OurIdGenerator++;
    }

private:
    static CommandId OurIdGenerator;

};

CommandId CommandRegistry::OurIdGenerator = 0;

template<class T>
CommandId CommandRegistry::Entry<T>::OurId = -1;

/// \brief example of command with two parameters
class Command_Example_A
{
public:
    struct Parameters
    {
        int a;
        float b;
    };

};

/// \brief another example of command with two parameters
class Command_Example_B
{
public:
    struct Parameters
    {
        int a;
        char b;
    };

};

/// \brief example of command with no parameter
class Command_Example_C
{
public:
    struct Parameters
    {

    };

};

/// \brief wraps everything needed to identify the command and to pass its parameters on
class Command
{
    /// \brief private part of a command used to wrap its parameters
    template <class T>
    class CommandParameters
    {
        using Parameters = typename T::Parameters;

    public:
        CommandParameters(const Parameters& parameters) :
            myParameters(parameters)
        {

        }

        template <class F>
        void execute(const F& executor) const
        {
            executor(myParameters);
        }

        const Parameters& parameters() const { return myParameters; }

    private:
        Parameters myParameters;

    };

    /// \brief unique_ptr with some kind of type erasure able to correctly delete the pointed object
    using CommandUniquePtr = std::unique_ptr<void, void(*)(const void*)>;

protected:
    Command(CommandId id, CommandUniquePtr&& commandPrivate) :
        myId(id),
        myCommandPrivate(std::move(commandPrivate))
    {

    }

public:
    template<class T, class... Args>
    static Command Create(Args... args)
    {
        return Create<T>({ args... });
    }

    template<class T>
    static Command Create(const typename T::Parameters& parameters)
    {
        return Command(CommandRegistry::Id<T>(), CommandUniquePtr(new CommandParameters<T>(parameters), [](const void* ptr) { delete static_cast<const CommandParameters<T>*>(ptr); }));
    }

    CommandId id() const { return myId; }

    template<class T, class F>
    void execute(const F& executor) const
    {
        static_cast<CommandParameters<T>*>(myCommandPrivate.get())->execute(executor);
    }

private:
    CommandId myId;
    CommandUniquePtr myCommandPrivate;

};

using Commands = std::vector<Command>;

/// \brief dispatch commands to pre-registered executors (method able to process a specific kind of command)
template <class T>
class Engine
{
public:
    Engine();

    /// \brief dispatch commands to their respective executor
    void dispatch(const Commands& commands);

protected:
    /// \brief register a specific kind of command, engine must define an execute<C> method
    template<class C>
    void registerCommand();

    /// \brief map a unknown command to its executor
    void route(const Command& command);

    /// \brief execute the mapped command
    template <class C>
    void execute(const Command& command);

protected:
    using Executor = void(Engine<T>::*)(const Command&);
    std::vector<Executor> myCommandMapper; /// \brief map between a command id and its executor

};

template <class T>
Engine<T>::Engine() :
    myCommandMapper()
{
    myCommandMapper.reserve(CommandRegistry::Count());
}

template <class T>
template<class C>
void Engine<T>::registerCommand()
{
    if(myCommandMapper.size() <= static_cast<std::size_t>(CommandRegistry::Id<C>()))
        myCommandMapper.resize(CommandRegistry::Id<C>() + 1);

    myCommandMapper[CommandRegistry::Id<C>()] = &Engine<T>::execute<C>;
}

template <class T>
void Engine<T>::dispatch(const Commands& commands)
{
    for(const Command& command : commands)
        route(command);
}

template <class T>
void Engine<T>::route(const Command& command)
{
    CommandId id = command.id();

    if(static_cast<std::size_t>(id) < myCommandMapper.size())
    {
        const Executor& executor = myCommandMapper[id];
        if(executor)
            return (this->*executor)(command);
    }

    throw std::runtime_error("Command unknown");
}

template <class T>
template <class C>
void Engine<T>::execute(const Command& command)
{
    command.execute<C>([this](const typename C::Parameters& parameters) { T* self = static_cast<T*>(this); self->template execute<C>(parameters); });
}

/// \brief example of a specific engine able to process our example commands, here it's supposed to be a rendering engine base on a QtOpenGL backend
class QtOpenGLRenderEngine : public Engine<QtOpenGLRenderEngine>
{
public:
    /// \brief define which commands this engine can process
    QtOpenGLRenderEngine();

public:
    /// \brief define the command executors
    template <class C>
    void execute(const typename C::Parameters& parameters);

};

QtOpenGLRenderEngine::QtOpenGLRenderEngine()
{
    registerCommand<Command_Example_A>();
    registerCommand<Command_Example_B>();
    registerCommand<Command_Example_C>();
}

template <>
void QtOpenGLRenderEngine::execute<Command_Example_A>(const Command_Example_A::Parameters& parameters)
{
    std::cout << "Executing 'Command_Example_A' with parameters: (" << parameters.a << ", " << parameters.b << ")" << std::endl;
}

template <>
void QtOpenGLRenderEngine::execute<Command_Example_B>(const Command_Example_B::Parameters& parameters)
{
    std::cout << "Executing 'Command_Example_B' with parameters: (" << parameters.a << ", " << parameters.b << ")" << std::endl;
}

template <>
void QtOpenGLRenderEngine::execute<Command_Example_C>(const Command_Example_C::Parameters&)
{
    std::cout << "Executing 'Command_Example_C' with no parameter" << std::endl;
}

int main()
{
    std::cout << "<app>" << std::endl << std::endl;
    {
        QtOpenGLRenderEngine engine;

        Commands commands;

        commands.push_back(Command::Create<Command_Example_A>(7, 3.2f));
        commands.push_back(Command::Create<Command_Example_B>(5, 'b'));
        commands.push_back(Command::Create<Command_Example_C>());

        engine.dispatch(commands);
    }
    std::cout << std::endl << "</app>" << std::endl;

    return 0;
}
