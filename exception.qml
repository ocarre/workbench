import QtQml
import QtQuick 2.15
import QtQuick.Window 2.15

QtObject {
    Component.onCompleted: {
        console.log("<test>");

        try
        {
            let good = new Promise((resolve, reject) => {
                console.log("  <good>");
                console.log("  </good>");
                resolve();
            });

            let bad = new Promise((resolve, reject) => {
                console.log("  <bad>");
                throw "blub"; // executor stop here, but nothing is ever printed so good luck with that
                console.log("  </bad>"); // this will never print
                resolve();
            });
        }
        catch(e)
        {
            // nope, exception in promise executor do not go this up
            console.error(e);
        }

        console.log("</test>");
    }
}
