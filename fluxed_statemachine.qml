EntryPoint {
    StateMachine {
        id: machine

        readonly property alias evaluateGotoAction: evaluateGoto
        ActionCreatorRegister {
            ActionCreator {
                id: evaluateGoto

                type: "EVALUATE_GOTO"
                signal dispatch(string fromState, string toState)
            }
        }

        function evaluateTransition(fromState, toState) {
            // find fromState and toState objects
            // check if a transition exists or error
            // evaluate transition conditions needing heavy computation or not
            // return a promise that may be already resolved / failed (synchronous evaluation) or pending (in case of async)
        }

        function activateTransition(fromState, toState) {
            // find fromState and toState objects
            // fire the according transition signal
        }

        // state and transition definition
    }

    Store {
        id: store

        machine: machine
        readonly property var modes: createModesFromMachine() // mode model based from mode data
        readonly property var tools: createToolsFromMachine() // tool model based from tool data
    }

    Middleware {
        filters: [
            ActionFilter {
                type: machine.evaluateGotoAction.request

                onDispatched: (action) {
                    var promise = machine.evaluateTransition(action.payload.fromState, action.payload.toState);
                    action.resolve(promise);
                }
            },
            ActionFilter {
                type: machine.evaluateGotoAction.success

                onDispatched: (action) {
                    var result = machine.action.payload.values[0];
                    
                    machine.activateTransition(result.fromState, result.toState);
                }
            },
            ActionFilter {
                type: machine.evaluateGotoAction.failure

                onDispatched: (action) {
                    console.log(action.error);
                }
            }
        ]
    }

    ModePanel {
        model: store.modes
        currentIndex: store.currentMode
        onClicked: (mode) => mode.goto()
        // TODO: use mode.enabled inside to activate or not the corresponding mode button
    }


    ToolPanel {
        model: store.tools
        currentTool: store.currentTool
        onClicked: (tool) => tool.goto()
        // TODO: use tool.enabled inside to activate or not the corresponding tool button
    }

```
}
