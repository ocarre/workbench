import QtQml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

Window {
    width: 200
    height: layout.implicitHeight
    visible: true

    ColumnLayout {
        id: layout
        anchors.fill: parent

        MouseArea {
            Layout.fillWidth: true
            implicitHeight: 100

            focus: true
            onPressed: forceActiveFocus();
            onActiveFocusChanged: if(!activeFocus) forceActiveFocus();

            Rectangle {
                anchors.fill: parent
                color: parent.activeFocus ? "green" : "red"
            }
        }

        Control {
            Layout.fillWidth: true
            implicitHeight: sub.implicitHeight

            focusPolicy: Qt.NoFocus

            ColumnLayout {
                id: sub
                anchors.fill: parent

                Button {
                    Layout.fillWidth: true

                    text: "A"
                }

                Button {
                    Layout.fillWidth: true

                    text: "B"
                }
            }
        }

        Button {
            Layout.fillWidth: true

            text: "C"
        }

        TextArea {
            Layout.fillWidth: true

            text: "Hello"
        }
    }
}
