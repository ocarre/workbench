#include <iostream>
#include <functional>
#include <memory>
#include <sstream>
#include <list>
#include <cassert>

#define DEBUG_MSG(x) std::cout << x
#define DEBUG_LOG(x) std::cout << x << std::endl
#define DEBUG_LOG_ENDL() std::cout << std::endl

using TypeIndex = std::size_t;

template<class T>
class TypeRegister
{
    static TypeIndex Generator;

public:
    template<class U>
    static TypeIndex Type()
    {
        static TypeIndex Id = Generator++;

        return Id;
    }

};

template<class T>
TypeIndex TypeRegister<T>::Generator = 0;

class Pipeline
{
public:
    class Stage
    {
    public:
        Stage(Pipeline& pipeline) :
            myPipeline(pipeline)
        {

        }

        void operator()()
        {
            myPipeline.next();
        }

    private:
        Pipeline& myPipeline;

    };

    using Continuation = std::function<void(Pipeline::Stage&)>;
    using Terminal = std::function<void()>;

public:
    Pipeline() :
        myContinuations(),
        myIterator(myContinuations.end()),
        myTerminal()
    {
        
    }

    template<class F>
    void add(F function)
    {
        if(running())
            throw std::runtime_error("Cannot add a continuation during pipeline execution");

        myContinuations.push_back(function);
    }

    void execute(const Terminal& terminal)
    {
        if(running())
            throw std::runtime_error("Pipeline already running");

        myTerminal = terminal;

        myIterator = myContinuations.begin();

        next();

        myTerminal = {};
    }

protected:
    bool running() const
    {
        return !!myTerminal;
    }

    void next()
    {
        if(!running())
            throw std::runtime_error("Pipeline is not running");
            
        const std::list<Continuation>::const_iterator current_iterator = myIterator++;

        if(myContinuations.end() != current_iterator)
        {
            Stage stage(*this);
            (*current_iterator)(stage);
            
            std::list<Continuation>::const_iterator next_iterator = current_iterator;
            next_iterator++;

            if(next_iterator == myIterator)
            {
                throw std::runtime_error("Pipeline::Stage call is missing");
            }
        }
        else
        {
            myTerminal();
        }
    }

private:
    std::list<Continuation> myContinuations;
    std::list<Continuation>::const_iterator myIterator;
    Terminal myTerminal;
    
};

class Node : public std::enable_shared_from_this<Node>
{
public:
    using Ptr = std::shared_ptr<Node>;

protected:
    Node(const std::string& name, Node::Ptr parent) :
        myParent(parent),
        myName(name)
    {
    
    }

public:
    virtual ~Node() = default;

    Node::Ptr parent() const { return myParent.lock(); }
    const std::string& name() const { return myName; }

private:
    const std::weak_ptr<Node> myParent;
    const std::string myName;

};

template<class T>
class DerivedNode : public Node
{
public:
    using Ptr = std::shared_ptr<T>;

protected:
    DerivedNode(const std::string& name, Node::Ptr parent) : Node(name, parent)
    {
    
    }

};

class Component;

class Entity : public DerivedNode<Entity>
{
public:
    using Ptr = std::shared_ptr<Entity>;
    using ComponentPtr = std::shared_ptr<Component>;

private:
    Entity(const std::string& name, Ptr parent = nullptr) : DerivedNode(name, parent),
        myParentEntity(parent),
        myChildren(),
        myComponents()
    {

    }

public:
    Entity::Ptr parentEntity() const { return myParentEntity.lock(); }
    const std::vector<Ptr>& children() const { return myChildren; }
    const std::vector<ComponentPtr>& components() const { return myComponents; }

    static Ptr CreateRoot(const std::string& name)
    {
        return Ptr(new Entity(name));
    }

    typename Entity::Ptr createChild(const std::string& name)
    {
        Entity::Ptr child(new Entity(name, static_pointer_cast<Entity>(shared_from_this())));

        myChildren.push_back(child);

        return child;
    }

    template<class T>
    typename T::Ptr createComponent(const std::string& name)
    {
        static_assert(std::is_base_of<Component, T>());

        typename T::Ptr component(new T(name, static_pointer_cast<Entity>(shared_from_this())));

        myComponents.push_back(component);

        return component;
    }

private:
    std::weak_ptr<Entity> myParentEntity;
    std::vector<Ptr> myChildren;
    std::vector<ComponentPtr> myComponents;

};

class FrameContext
{
public:
    void add(const std::string& value)
    {        
        myResult += value;
    }

    const std::string& result() const { return myResult; }

private:
    std::string myResult;

};

class FrameGraphNodeContext;

class FrameGraphNode : public DerivedNode<FrameGraphNode>
{
public:
    using Ptr = std::shared_ptr<FrameGraphNode>;

    template<class T> friend class DerivedFrameGraphNode;

private:
    FrameGraphNode(const std::string& name, Ptr parent = nullptr) : DerivedNode(name, parent),
        myParentFrameGraphNode(parent),
        myChildren()
    {

    }

public:
    FrameGraphNode::Ptr parentFrameGraphNode() const { return myParentFrameGraphNode.lock(); }
    const std::vector<Ptr>& children() const { return myChildren; }

    static Ptr CreateRoot(const std::string& name)
    {
        return Ptr(new FrameGraphNode(name));
    }

    template<class T>
    typename T::Ptr createChild(const std::string& name)
    {
        static_assert(std::is_base_of<FrameGraphNode, T>());

        typename T::Ptr child(new T(name, static_pointer_cast<FrameGraphNode>(shared_from_this())));

        myChildren.push_back(child);

        return child;
    }

    virtual void generate(FrameContext&, const FrameGraphNodeContext&, Entity::Ptr&) {}

    template<class T>
    static TypeIndex Type();
    virtual TypeIndex type() const;

public:
    static std::string_view ClassName() { return "FrameGraphNode"; }
    virtual std::string_view className() const { return ClassName(); }

private:
    std::weak_ptr<FrameGraphNode> myParentFrameGraphNode;
    std::vector<Ptr> myChildren;

};

class EntityContext
{
public:
    explicit EntityContext(const Entity::Ptr& entity) :
        myId(entity->name()),
        myEntities()
    {
        
    }

    EntityContext(const EntityContext& parentContext, const Entity::Ptr& entity) :
        myId(parentContext.myId + ", " + entity->name()),
        myEntities(parentContext.myEntities)
    {
        
    }

    const std::string& id() const { return myId; }
    const Entity::Ptr& entity() const { return myEntities.back(); }

private:
    const std::string myId;

    std::vector<Entity::Ptr> myEntities; // [EntityType]

};

class Component : public DerivedNode<Component>
{
public:
    using Ptr = std::shared_ptr<Component>;

    template<class T> friend class DerivedComponent;

    using Behavior = std::function<void(const FrameGraphNode::Ptr&, FrameContext&, const FrameGraphNodeContext&, const EntityContext&)>;

private:
    Component(const std::string& name, Entity::Ptr parent = nullptr) : DerivedNode(name, parent)
    {

    }

public:
    template<class C>
    void generate(const typename C::Ptr& frameGraphNode, FrameContext& frameContext, const FrameGraphNodeContext& frameGraphNodeContext, const EntityContext& entityContext)
    {
        DEBUG_LOG("Generate " << className() << " -> " << C::ClassName() << " (" << TypeRegister<Component>::Type<C>() << ")");

        const auto behavior = fetch<C>();
        if(behavior)
        {
            behavior(frameGraphNode, frameContext, frameGraphNodeContext, entityContext);
        }
    }

protected:
    template<class C, class F>
    void add(F functor)
    {
        const auto behavior = [functor](const FrameGraphNode::Ptr& frameGraphNode, FrameContext& frameContext, const FrameGraphNodeContext& frameGraphNodeContext, const EntityContext& entityContext) {
            return functor(static_pointer_cast<C>(frameGraphNode), frameContext, frameGraphNodeContext, entityContext);
        };

        fetch<C>() = behavior;
    }

private:
    template<class C>
    Behavior& fetch() const
    {
        const TypeIndex typeIndex = TypeRegister<Component>::Type<C>();

        if(typeIndex >= myBehaviors.size())
        {
            myBehaviors.resize(typeIndex + 1);
        }

        return myBehaviors[typeIndex];
    }

public:
    template<class T>
    static TypeIndex Type();
    virtual TypeIndex type() const = 0;

    static std::string_view ClassName() { return "Component"; }
    virtual std::string_view className() const { return ClassName(); }

private:
    mutable std::vector<Behavior> myBehaviors;

};

template<class T>
class DerivedComponent : public Component
{
public:
    using Ptr = std::shared_ptr<T>;
    
protected:
    DerivedComponent(const std::string& name, Entity::Ptr parent) : Component(name, parent)
    {
    
    }

public:
    TypeIndex type() const override;

};

template<class T>
TypeIndex Component::Type()
{
    return TypeRegister<Component>::Type<T>();
}

template<class T>
TypeIndex DerivedComponent<T>::type() const
{
    return TypeRegister<Component>::Type<T>();
}

template<class T>
class DerivedFrameGraphNode : public FrameGraphNode
{
public:
    using Ptr = std::shared_ptr<T>;

protected:
    DerivedFrameGraphNode(const std::string& name, FrameGraphNode::Ptr parent) : FrameGraphNode(name, parent)
    {
    
    }

public:
    TypeIndex type() const override;

};

template<class T>
class FrameGraphNodeBinder
{
public:
    void bind(Pipeline::Stage&, FrameContext&, const FrameGraphNodeContext&);

};

class FrameGraphNodeA : public DerivedFrameGraphNode<FrameGraphNodeA>, public FrameGraphNodeBinder<FrameGraphNodeA>
{
    friend class FrameGraphNode;

public:
    using Ptr = std::shared_ptr<FrameGraphNodeA>;
    static std::string_view ClassName() { return "FrameGraphNodeA"; }
    std::string_view className() const override { return ClassName(); }
    
protected:
    FrameGraphNodeA(const std::string& name, FrameGraphNode::Ptr parent) : DerivedFrameGraphNode(name, parent)
    {

    }

public:
    void bind(Pipeline::Stage&, FrameContext&, const FrameGraphNodeContext&);

};

class FrameGraphNodeB: public DerivedFrameGraphNode<FrameGraphNodeB>, public FrameGraphNodeBinder<FrameGraphNodeB>
{
    friend class FrameGraphNode;

public:
    using Ptr = std::shared_ptr<FrameGraphNodeB>;
    static std::string_view ClassName() { return "FrameGraphNodeB"; }
    std::string_view className() const override { return ClassName(); }

protected:
    FrameGraphNodeB(const std::string& name, FrameGraphNode::Ptr parent) : DerivedFrameGraphNode(name, parent)
    {

    }

public:
    void bind(Pipeline::Stage&, FrameContext&, const FrameGraphNodeContext&);

};

class FrameGraphNodeC : public DerivedFrameGraphNode<FrameGraphNodeC>
{
    friend class FrameGraphNode;

public:
    using Ptr = std::shared_ptr<FrameGraphNodeC>;
    static std::string_view ClassName() { return "FrameGraphNodeC"; }
    std::string_view className() const override { return ClassName(); }

protected:
    FrameGraphNodeC(const std::string& name, FrameGraphNode::Ptr parent) : DerivedFrameGraphNode(name, parent)
    {

    }

public:
    void generate(FrameContext&, const FrameGraphNodeContext&, Entity::Ptr&) override;

};

class ComponentA : public DerivedComponent<ComponentA>
{
    friend class Entity;

private:
    ComponentA(const std::string& name, Entity::Ptr parent = nullptr) : DerivedComponent(name, parent)
    {
        add<FrameGraphNodeC>([this](const FrameGraphNodeC::Ptr& frameGraphNodeC, FrameContext&, const FrameGraphNodeContext&, const EntityContext&) {
            DEBUG_LOG(">>>>> " << className() << " / " << frameGraphNodeC->className() << " implementation");
        });
    }

public:
    static std::string_view ClassName() { return "ComponentA"; }
    std::string_view className() const override { return ClassName(); }

};

class ComponentB : public DerivedComponent<ComponentB>
{
    friend class Entity;

private:
    ComponentB(const std::string& name, Entity::Ptr parent = nullptr) : DerivedComponent(name, parent)
    {
        add<FrameGraphNodeC>([this](const FrameGraphNodeC::Ptr& frameGraphNodeC, FrameContext&, const FrameGraphNodeContext&, const EntityContext&) {
            DEBUG_LOG(">>>>> " << className() << " / " << frameGraphNodeC->className() << " implementation");
        });
    }

public:
    static std::string_view ClassName() { return "ComponentB"; }
    std::string_view className() const override { return ClassName(); }

};

template<class T>
TypeIndex FrameGraphNode::Type()
{
    return TypeRegister<FrameGraphNode>::Type<T>();
}

TypeIndex FrameGraphNode::type() const
{
    return TypeRegister<FrameGraphNode>::Type<FrameGraphNode>();
}

template<class T>
TypeIndex DerivedFrameGraphNode<T>::type() const
{
    return TypeRegister<FrameGraphNode>::Type<T>();
}

class FrameGraphNodeB;

class FrameGraphNodeContext
{
public:
    explicit FrameGraphNodeContext(const FrameGraphNode::Ptr& frameGraphNode) :
        myFrameGraphNode(frameGraphNode),
        myId(frameGraphNode->name()),
        myFrameGraphNodes()
    {
        add(frameGraphNode);
    }

    FrameGraphNodeContext(const FrameGraphNodeContext& parentContext, const FrameGraphNode::Ptr& frameGraphNode) :
        myFrameGraphNode(frameGraphNode),
        myId(parentContext.myId + ", " + frameGraphNode->name()),
        myFrameGraphNodes(parentContext.myFrameGraphNodes)
    {
        add(frameGraphNode);
    }

    const FrameGraphNode::Ptr& frameGraphNode() const { return myFrameGraphNode; }
    const std::string& id() const { return myId; }

    template<class T>
    const std::vector<typename T::Ptr>& fetch() const
    {
        const TypeIndex typeIndex = FrameGraphNode::Type<T>();

        return reinterpret_cast<const std::vector<typename T::Ptr>&>(fetch(typeIndex));
    }

private:
    std::vector<FrameGraphNode::Ptr>& fetch(TypeIndex typeIndex) const
    {
        if(typeIndex >= myFrameGraphNodes.size())
        {
            myFrameGraphNodes.resize(typeIndex + 1);
        }
        
        return myFrameGraphNodes[typeIndex];
    }

    void add(const FrameGraphNode::Ptr& frameGraphNode)
    {
        const TypeIndex typeIndex = frameGraphNode->type();

        fetch(typeIndex).push_back(frameGraphNode);
    }

private:
    const FrameGraphNode::Ptr& myFrameGraphNode;
    const std::string myId;

    mutable std::vector<std::vector<FrameGraphNode::Ptr>> myFrameGraphNodes; // [FrameGraphNodeType]

};

template<class GeneratorType, class T, class ... Args>
class GeneratorRegister
{
public:
    using Wrapper = std::function<GeneratorType(const typename T::Ptr&, const typename Args::Ptr& ...)>;

public:
    static std::string FormatArgs()
    {
        std::string fmt;

        int i = 0;
        ((fmt += std::string(Args::ClassName()) + " (" + std::to_string(Args::template Type<Args>()) + ")" + (sizeof...(Args) == ++i ? " " : ", ")), ...);

        return fmt;
    }

    template<class X, class F>
    static int Add(const F& generator)
    {
        const TypeIndex typeIndex = T::template Type<X>();
        Get(typeIndex) = [generator](const typename T::Ptr& key, const typename Args::Ptr& ... others) { return generator(static_pointer_cast<X>(key), others...); };

        DEBUG_LOG("Add: " << " " << X::ClassName() << " (" << typeIndex << ")");
        if constexpr(sizeof...(Args) > 0)
        {
            DEBUG_MSG(" -> " << FormatArgs());
        }
        DEBUG_LOG("");

        return 0;
    }
    
    static std::optional<GeneratorType> Get(const typename T::Ptr& key, const typename Args::Ptr& ... others)
    {
        const TypeIndex typeIndex = key->type();

        Wrapper& wrapper = Get(typeIndex);
        if(wrapper)
            return wrapper(key, others...);

        return {};
    }

private:
    static Wrapper& Get(TypeIndex typeIndex)
    {
        if(typeIndex >= OurGenerators.size())
            OurGenerators.resize(typeIndex + 1);

        return OurGenerators[typeIndex];
    }

private:
    static std::vector<Wrapper> OurGenerators;

};

template<class GeneratorType, class T, class ... Args>
std::vector<typename GeneratorRegister<GeneratorType, T, Args...>::Wrapper> GeneratorRegister<GeneratorType, T, Args...>::OurGenerators = {};

template<class T>
class Visitor
{
public:
    template<class F>
    void visit(const typename T::Ptr& node, const F& visitor)
    {
        visitor(node);

        for(const typename T::Ptr& child : node->children())
        {
            visit(child, visitor);
        }
    }

};

template<class T>
class Propagator
{
public:
    template<class C, class F>
    void propagate(const typename T::Ptr& node, const F& propagator)
    {
        propagate<C>(node, propagator, [](const typename T::Ptr& root) { return C(root); });
    }

    template<class C, class F, class I>
    void propagate(const typename T::Ptr& node, const F& propagator, const I& initer)
    {
        const C& context = propagator(initer(node), node);

        for(auto child : node->children())
        {
            propagateToChildren(child, propagator, context);
        }
    }

protected:
    template<class C, class F>
    void propagateToChildren(const typename T::Ptr& node, const F& propagator, const C& parentContext)
    {
        const C& context = propagator(parentContext, node);

        for(auto child : node->children())
        {
            propagateToChildren(child, propagator, context);
        }
    }

};

using ComponentGenerator = decltype(std::function<void(FrameContext&, const FrameGraphNodeContext&, const EntityContext&)>());

using FrameGraphGenerator = decltype(std::function<void(FrameContext&, const FrameGraphNodeContext&, const Entity::Ptr&)>());
using FrameGraphStateGenerator = decltype(std::function<void(Pipeline::Stage& next, FrameContext&, const FrameGraphNodeContext&, const Entity::Ptr&)>());

namespace
{

template<class T>
void AddLastBindableToPipeline(Pipeline& pipeline, FrameContext& frameContext, const FrameGraphNodeContext& frameGraphNodeContext)
{
    const std::vector<typename T::Ptr>& frameGraphNodes = frameGraphNodeContext.fetch<T>();
    if(frameGraphNodes.empty())
        return;

    const typename T::Ptr& frameGraphNode = frameGraphNodes.back();

    pipeline.add([&](Pipeline::Stage& next) {
        frameGraphNode->bind(next, frameContext, frameGraphNodeContext);
    });
}

}

void FrameGraphNodeA::bind(Pipeline::Stage& next, FrameContext&, const FrameGraphNodeContext&)
{
    DEBUG_LOG("<Binding> " << className());

    next();

    DEBUG_LOG("</Binding> " << className());
}

void FrameGraphNodeB::bind(Pipeline::Stage& next, FrameContext&, const FrameGraphNodeContext&)
{
    DEBUG_LOG("<Binding> " << className());

    next();

    DEBUG_LOG("</Binding> " << ClassName());
}

void FrameGraphNodeC::generate(FrameContext& frameContext, const FrameGraphNodeContext& frameGraphNodeContext, Entity::Ptr& entity)
{
    DEBUG_LOG("<" << className() << ">");

    Pipeline pipeline;

    AddLastBindableToPipeline<FrameGraphNodeA>(pipeline, frameContext, frameGraphNodeContext);
    AddLastBindableToPipeline<FrameGraphNodeB>(pipeline, frameContext, frameGraphNodeContext);

    pipeline.execute([&]() {
        DEBUG_LOG("<Execute>");
        Propagator<Entity>().propagate<EntityContext>(entity, [&frameContext, &frameGraphNodeContext, frameGraphNodeC = static_pointer_cast<FrameGraphNodeC>(shared_from_this())](const EntityContext& parentEntityContext, const Entity::Ptr& entity) {
            EntityContext entityContext(parentEntityContext, entity);

            for(const Component::Ptr& component : entity->components())
            {
                component->generate<FrameGraphNodeC>(frameGraphNodeC, frameContext, frameGraphNodeContext, entityContext);
            }

            return entityContext;
        });
        DEBUG_LOG("</Execute>");
    });

    DEBUG_LOG("</" << className() << ">");
}

int main() try
{
    DEBUG_LOG_ENDL();

// graph description

    auto f_root = FrameGraphNode::CreateRoot("f_root");
    {
        auto f_a = f_root->createChild<FrameGraphNodeA>("f_a");
        {
            auto f_aa = f_a->createChild<FrameGraphNodeB>("f_aa");
            {
                auto f_aaa = f_aa->createChild<FrameGraphNodeC>("f_aaa");
            }
        }
        // auto f_b = f_root->createChild<FrameGraphNodeB>("f_b");
        // auto f_c = f_root->createChild<FrameGraphNodeA>("f_c");
        // {
        //     auto f_ca = f_c->createChild<FrameGraphNodeC>("f_ca");
        // }
    }

// graph print

    Visitor<FrameGraphNode>().visit(f_root, [](FrameGraphNode::Ptr frameGraphNode) {
        std::stringstream indentation;
        for(FrameGraphNode::Ptr parent = frameGraphNode->parentFrameGraphNode(); parent; parent = parent->parentFrameGraphNode())
        {
            indentation << "| ";
        }

        DEBUG_LOG(indentation.str() << frameGraphNode->className() << " - " << frameGraphNode->name());
    });

// graph description

    auto e_root = Entity::CreateRoot("e_root");
    {
        auto e_a = e_root->createChild("e_a");
        {
            auto c_a = e_a->createComponent<ComponentA>("c_aa");
            auto c_b = e_a->createComponent<ComponentB>("c_ab");
        }

        auto e_b = e_root->createChild("e_b");
        {
            auto c_a = e_b->createComponent<ComponentA>("c_ba");
        }
    }

    DEBUG_LOG_ENDL();

// graph generate

    FrameContext frameContext;
    Propagator<FrameGraphNode>().propagate<FrameGraphNodeContext>(f_root, [&frameContext, &e_root](const FrameGraphNodeContext& parentFrameGraphNodeContext, const FrameGraphNode::Ptr& frameGraphNode) {
        FrameGraphNodeContext frameGraphNodeContext(parentFrameGraphNodeContext, frameGraphNode);

        frameGraphNode->generate(frameContext, frameGraphNodeContext, e_root);

        return frameGraphNodeContext;
    });

    DEBUG_LOG(frameContext.result());

    return 0;
}
catch(const std::exception& e)
{
    std::cerr << "exception caught: " << e.what() << std::endl;

    return 1;
}
