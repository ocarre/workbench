#include <iostream>
#include <functional>
#include <memory>
#include <sstream>
#include <list>
#include <cassert>

#define DEBUG_LOG(x) std::cout << x << std::endl

using TypeIndex = std::size_t;

template<class T>
class TypeRegister
{
    static TypeIndex Generator;

public:
    template<class U>
    static TypeIndex Type()
    {
        static TypeIndex Id = Generator++;

        return Id;
    }

};

template<class T>
TypeIndex TypeRegister<T>::Generator = 0;

class Node : public std::enable_shared_from_this<Node>
{
public:
    using Ptr = std::shared_ptr<Node>;

protected:
    Node(const std::string& name, Node::Ptr parent) :
        myParent(parent),
        myName(name)
    {
    
    }

public:
    virtual ~Node() = default;

    Node::Ptr parent() const { return myParent.lock(); }
    const std::string& name() const { return myName; }

private:
    const std::weak_ptr<Node> myParent;
    const std::string myName;

};

template<class T>
class DerivedNode : public Node
{
public:
    using Ptr = std::shared_ptr<T>;

protected:
    DerivedNode(const std::string& name, Node::Ptr parent) : Node(name, parent)
    {
    
    }

};

class Component;

class Entity : public DerivedNode<Entity>
{
public:
    using Ptr = std::shared_ptr<Entity>;
    using ComponentPtr = std::shared_ptr<Component>;

private:
    Entity(const std::string& name, Ptr parent = nullptr) : DerivedNode(name, parent),
        myParentEntity(parent),
        myChildren(),
        myComponents()
    {

    }

public:
    Entity::Ptr parentEntity() const { return myParentEntity.lock(); }
    const std::vector<Ptr>& children() const { return myChildren; }
    const std::vector<ComponentPtr>& components() const { return myComponents; }

    static Ptr CreateRoot(const std::string& name)
    {
        return Ptr(new Entity(name));
    }

    typename Entity::Ptr createChild(const std::string& name)
    {
        Entity::Ptr child(new Entity(name, static_pointer_cast<Entity>(shared_from_this())));

        myChildren.push_back(child);

        return child;
    }

    template<class T>
    typename T::Ptr createComponent(const std::string& name)
    {
        static_assert(std::is_base_of<Component, T>());

        typename T::Ptr component(new T(name, static_pointer_cast<Entity>(shared_from_this())));

        myComponents.push_back(component);

        return component;
    }

private:
    std::weak_ptr<Entity> myParentEntity;
    std::vector<Ptr> myChildren;
    std::vector<ComponentPtr> myComponents;

};

class Component : public DerivedNode<Component>
{
public:
    using Ptr = std::shared_ptr<Component>;

    template<class T> friend class DerivedComponent;

private:
    Component(const std::string& name, Entity::Ptr parent = nullptr) : DerivedNode(name, parent)
    {

    }

public:
    template<class T>
    static TypeIndex Type();
    virtual TypeIndex type() const = 0;

    static std::string_view ClassName() { return "Component"; }
    virtual std::string_view className() const { return ClassName(); }

};

template<class T>
class DerivedComponent : public Component
{
public:
    using Ptr = std::shared_ptr<T>;
    
protected:
    DerivedComponent(const std::string& name, Entity::Ptr parent) : Component(name, parent)
    {
    
    }

public:
    TypeIndex type() const override;

};

template<class T>
TypeIndex Component::Type()
{
    return TypeRegister<Component>::Type<T>();
}

template<class T>
TypeIndex DerivedComponent<T>::type() const
{
    return TypeRegister<Component>::Type<T>();
}

class ComponentA : public DerivedComponent<ComponentA>
{
    friend class Entity;

private:
    ComponentA(const std::string& name, Entity::Ptr parent = nullptr) : DerivedComponent(name, parent)
    {

    }

public:
    static std::string_view ClassName() { return "ComponentA"; }
    std::string_view className() const override { return ClassName(); }

};

class ComponentB : public DerivedComponent<ComponentB>
{
    friend class Entity;

private:
    ComponentB(const std::string& name, Entity::Ptr parent = nullptr) : DerivedComponent(name, parent)
    {
    
    }

public:
    static std::string_view ClassName() { return "ComponentB"; }
    std::string_view className() const override { return ClassName(); }

};

class FrameContext
{
public:
    void add(const std::string& value)
    {        
        myResult += value;
    }

    const std::string& result() const { return myResult; }

private:
    std::string myResult;

};

class FrameGraphNodeContext;

class FrameGraphNode : public DerivedNode<FrameGraphNode>
{
public:
    using Ptr = std::shared_ptr<FrameGraphNode>;

    template<class T> friend class DerivedFrameGraphNode;

private:
    FrameGraphNode(const std::string& name, Ptr parent = nullptr) : DerivedNode(name, parent),
        myParentFrameGraphNode(parent),
        myChildren()
    {

    }

public:
    FrameGraphNode::Ptr parentFrameGraphNode() const { return myParentFrameGraphNode.lock(); }
    const std::vector<Ptr>& children() const { return myChildren; }

    static Ptr CreateRoot(const std::string& name)
    {
        return Ptr(new FrameGraphNode(name));
    }

    template<class T>
    typename T::Ptr createChild(const std::string& name)
    {
        static_assert(std::is_base_of<FrameGraphNode, T>());

        typename T::Ptr child(new T(name, static_pointer_cast<FrameGraphNode>(shared_from_this())));

        myChildren.push_back(child);

        return child;
    }

    template<class T>
    static TypeIndex Type();
    virtual TypeIndex type() const;

public:
    static std::string_view ClassName() { return "FrameGraphNode"; }
    virtual std::string_view className() const { return ClassName(); }

private:
    std::weak_ptr<FrameGraphNode> myParentFrameGraphNode;
    std::vector<Ptr> myChildren;

};

template<class T>
class DerivedFrameGraphNode : public FrameGraphNode
{
public:
    using Ptr = std::shared_ptr<T>;

protected:
    DerivedFrameGraphNode(const std::string& name, FrameGraphNode::Ptr parent) : FrameGraphNode(name, parent)
    {
    
    }

public:
    TypeIndex type() const override;

};

template<class T>
TypeIndex FrameGraphNode::Type()
{
    return TypeRegister<FrameGraphNode>::Type<T>();
}

TypeIndex FrameGraphNode::type() const
{
    return TypeRegister<FrameGraphNode>::Type<FrameGraphNode>();
}

template<class T>
TypeIndex DerivedFrameGraphNode<T>::type() const
{
    return TypeRegister<FrameGraphNode>::Type<T>();
}

class FrameGraphNodeB;

class FrameGraphNodeContext
{
public:
    explicit FrameGraphNodeContext(const FrameGraphNode::Ptr& frameGraphNode) :
        myFrameGraphNode(frameGraphNode),
        myId(frameGraphNode->name()),
        myFrameGraphNodes()
    {
        // std::cout << "+FrameGraphNodeContext: " << myFrameGraphNode->name() << " - " << myId << std::endl;

        add(frameGraphNode);
    }

    FrameGraphNodeContext(const FrameGraphNodeContext& parentContext, const FrameGraphNode::Ptr& frameGraphNode) :
        myFrameGraphNode(frameGraphNode),
        myId(parentContext.myId + ", " + frameGraphNode->name()),
        myFrameGraphNodes(parentContext.myFrameGraphNodes)
    {
        // std::cout << "-FrameGraphNodeContext: " << myFrameGraphNode->name() << " - " << myId << std::endl;

        add(frameGraphNode);
    }

    const FrameGraphNode::Ptr& frameGraphNode() const { return myFrameGraphNode; }
    const std::string& id() const { return myId; }

    template<class T>
    const std::vector<typename T::Ptr>& get() const
    {
        const TypeIndex typeIndex = FrameGraphNode::Type<T>();

        // std::cout << "get: " << T::ClassName() << " - " << typeIndex << " - " << myFrameGraphNodes[typeIndex].size() << std::endl;

        return reinterpret_cast<const std::vector<typename T::Ptr>&>(get(typeIndex));
    }

private:
    std::vector<FrameGraphNode::Ptr>& get(TypeIndex typeIndex) const
    {
        if(typeIndex >= myFrameGraphNodes.size())
        {
            myFrameGraphNodes.resize(typeIndex + 1);
        }
        
        return myFrameGraphNodes[typeIndex];
    }

    void add(const FrameGraphNode::Ptr& frameGraphNode)
    {
        const TypeIndex typeIndex = frameGraphNode->type();

        get(typeIndex).push_back(frameGraphNode);

        // std::cout << "add: " << frameGraphNode->className() << " - " << typeIndex << " - " << myFrameGraphNodes.size() << std::endl;
    }

private:
    const FrameGraphNode::Ptr& myFrameGraphNode;
    const std::string myId;

    mutable std::vector<std::vector<FrameGraphNode::Ptr>> myFrameGraphNodes; // [FrameGraphNodeType]

};

class FrameGraphNodeA : public DerivedFrameGraphNode<FrameGraphNodeA>
{
    friend class FrameGraphNode;

public:
    using Ptr = std::shared_ptr<FrameGraphNodeA>;
    static std::string_view ClassName() { return "FrameGraphNodeA"; }
    std::string_view className() const override { return ClassName(); }
    
protected:
    FrameGraphNodeA(const std::string& name, FrameGraphNode::Ptr parent) : DerivedFrameGraphNode(name, parent)
    {

    }

};

class FrameGraphNodeB: public DerivedFrameGraphNode<FrameGraphNodeB>
{
    friend class FrameGraphNode;

public:
    using Ptr = std::shared_ptr<FrameGraphNodeB>;
    static std::string_view ClassName() { return "FrameGraphNodeB"; }
    std::string_view className() const override { return ClassName(); }

protected:
    FrameGraphNodeB(const std::string& name, FrameGraphNode::Ptr parent) : DerivedFrameGraphNode(name, parent)
    {

    }

};

class FrameGraphNodeC : public DerivedFrameGraphNode<FrameGraphNodeC>
{
    friend class FrameGraphNode;

public:
    using Ptr = std::shared_ptr<FrameGraphNodeC>;
    static std::string_view ClassName() { return "FrameGraphNodeC"; }
    std::string_view className() const override { return ClassName(); }

protected:
    FrameGraphNodeC(const std::string& name, FrameGraphNode::Ptr parent) : DerivedFrameGraphNode(name, parent)
    {

    }

};

class EntityContext
{
public:
    explicit EntityContext(const Entity::Ptr& entity) :
        myId(entity->name()),
        myEntities()
    {
        // std::cout << "+EntityContext: " << myEntity->name() << " - " << myId << std::endl;
    }

    EntityContext(const EntityContext& parentContext, const Entity::Ptr& entity) :
        myId(parentContext.myId + ", " + entity->name()),
        myEntities(parentContext.myEntities)
    {
        // std::cout << "-EntityContext: " << myEntity->name() << " - " << myId << std::endl;
    }

    const std::string& id() const { return myId; }
    const Entity::Ptr& entity() const { return myEntities.back(); }

private:
    const std::string myId;

    std::vector<Entity::Ptr> myEntities; // [EntityType]

};

template<class GeneratorType, class T, class ... Args>
class GeneratorRegister
{
public:
    using Wrapper = std::function<GeneratorType(const typename T::Ptr&, const typename Args::Ptr& ...)>;

public:
    static std::string FormatArgs()
    {
        std::string fmt;

        int i = 0;
        ((fmt += std::string(Args::ClassName()) + " (" + std::to_string(Args::template Type<Args>()) + ")" + (sizeof...(Args) == ++i ? " " : ", ")), ...);

        return fmt;
    }

    template<class X, class F>
    static int Add(const F& generator)
    {
        const TypeIndex typeIndex = T::template Type<X>();
        Get(typeIndex) = [generator](const typename T::Ptr& key, const typename Args::Ptr& ... others) { return generator(static_pointer_cast<X>(key), others...); };

        std::cout << "Add: " << " " << X::ClassName() << " (" << typeIndex << ")";
        if constexpr(sizeof...(Args) > 0)
        {
            std::cout << " -> " << FormatArgs();
        }
        std::cout << std::endl;

        return 0;
    }
    
    static std::optional<GeneratorType> Get(const typename T::Ptr& key, const typename Args::Ptr& ... others)
    {
        const TypeIndex typeIndex = key->type();

        // std::cout << "Get: " << " " << key->className() << " (" << typeIndex << ")" << std::endl; // -> " << U::ClassName() << " (" << U::template Type<U>() << ")" << std::endl;

        Wrapper& wrapper = Get(typeIndex);
        if(wrapper)
            return wrapper(key, others...);

        return {};
    }

private:
    static Wrapper& Get(TypeIndex typeIndex)
    {
        if(typeIndex >= OurGenerators.size())
            OurGenerators.resize(typeIndex + 1);

        return OurGenerators[typeIndex];
    }

private:
    static std::vector<Wrapper> OurGenerators;

};

template<class GeneratorType, class T, class ... Args>
std::vector<typename GeneratorRegister<GeneratorType, T, Args...>::Wrapper> GeneratorRegister<GeneratorType, T, Args...>::OurGenerators = {};

template<class T>
class Visitor
{
public:
    template<class F>
    void visit(const typename T::Ptr& node, const F& visitor)
    {
        visitor(node);

        for(const typename T::Ptr& child : node->children())
        {
            visit(child, visitor);
        }
    }
};

template<class T>
class Propagator
{
public:
    template<class C, class F>
    void propagate(const typename T::Ptr& node, const F& propagator)
    {
        propagate<C>(node, propagator, [](const typename T::Ptr& root) { return C(root); });
    }

    template<class C, class F, class I>
    void propagate(const typename T::Ptr& node, const F& propagator, const I& initer)
    {
        const C& context = initer(node);

        for(auto child : node->children())
        {
            propagateToChildren(child, propagator, context);
        }
    }

protected:
    template<class C, class F>
    void propagateToChildren(const typename T::Ptr& node, const F& propagator, const C& parentContext)
    {
        const C& context = propagator(parentContext, node);

        for(auto child : node->children())
        {
            propagateToChildren(child, propagator, context);
        }
    }

};

class Pipeline
{
public:
    class Stage
    {
    public:
        Stage(Pipeline& pipeline) :
            myPipeline(pipeline)
        {

        }

        void operator()()
        {
            myPipeline.next();
        }

    private:
        Pipeline& myPipeline;

    };

    using Continuation = std::function<void(Pipeline::Stage&)>;
    using Terminal = std::function<void()>;

public:
    Pipeline() :
        myContinuations(),
        myIterator(myContinuations.end()),
        myTerminal()
    {
        
    }

    template<class F>
    void add(F function)
    {
        if(running())
            throw std::runtime_error("Cannot add a continuation during pipeline execution");

        myContinuations.push_back(function);
    }

    void execute(const Terminal& terminal)
    {
        if(running())
            throw std::runtime_error("Pipeline already running");

        myTerminal = terminal;

        myIterator = myContinuations.begin();

        next();

        myTerminal = {};
    }

protected:
    bool running() const { return !!myTerminal; }

    void next()
    {
        if(!running())
            throw std::runtime_error("Pipeline is not running");
            
        const std::list<Continuation>::const_iterator current_iterator = myIterator++;

        if(myContinuations.end() != current_iterator)
        {
            Stage stage(*this);
            (*current_iterator)(stage);
            
            std::list<Continuation>::const_iterator next_iterator = current_iterator;
            next_iterator++;

            if(next_iterator == myIterator)
            {
                throw std::runtime_error("Pipeline::Stage call is missing");
            }
        }
    }

private:
    std::list<Continuation> myContinuations;
    std::list<Continuation>::const_iterator myIterator;
    Terminal myTerminal;
    
};

using ComponentGenerator = decltype(std::function<void(FrameContext&, const FrameGraphNodeContext&, const EntityContext&)>());

static const int Generator_ComponentA_FrameGraphNodeC = GeneratorRegister<ComponentGenerator, Component, FrameGraphNodeC>::Add<ComponentA>([](const ComponentA::Ptr& componentA, const FrameGraphNodeC::Ptr& frameGraphNodeC) {
    return [componentA, frameGraphNodeC](FrameContext&, const FrameGraphNodeContext&, const EntityContext&)
    {
        std::cout << ">>>>> " << componentA->className() << " / " << frameGraphNodeC->className() << " implementation" << std::endl;
    };
});

static const int Generator_ComponentB_FrameGraphNodeC = GeneratorRegister<ComponentGenerator, Component, FrameGraphNodeC>::Add<ComponentB>([](const ComponentB::Ptr& componentB, const FrameGraphNodeC::Ptr& frameGraphNodeC) {
    return [componentB, frameGraphNodeC](FrameContext&, const FrameGraphNodeContext&, const EntityContext&)
    {
        std::cout << ">>>>> " << componentB->className() << " / " << frameGraphNodeC->className() << " implementation" << std::endl;
    };
});

static const int Generator_ComponentB_FrameGraphNodeA = GeneratorRegister<ComponentGenerator, Component, FrameGraphNodeA>::Add<ComponentB>([](const ComponentB::Ptr& componentB, const FrameGraphNodeA::Ptr& frameGraphNodeA) {
    return [componentB, frameGraphNodeA](FrameContext&, const FrameGraphNodeContext&, const EntityContext&)
    {
        std::cout << ">>>>> " << componentB->className() << " / " << frameGraphNodeA->className() << " implementation" << std::endl;
    };
});

using FrameGraphGenerator = decltype(std::function<void(FrameContext&, const FrameGraphNodeContext&, const Entity::Ptr&)>());
using FrameGraphStateGenerator = decltype(std::function<void(Pipeline::Stage& next, FrameContext&, const FrameGraphNodeContext&, const Entity::Ptr&)>());

namespace
{

template<class T>
void AddLastToPipeline(Pipeline& pipeline, FrameContext& frameContext, const FrameGraphNodeContext& frameGraphNodeContext, const Entity::Ptr& entity)
{
    const std::vector<typename T::Ptr>& frameGraphNodes = frameGraphNodeContext.get<T>();
    if(frameGraphNodes.empty())
        return;

    const typename T::Ptr& frameGraphNode = frameGraphNodes.back();

    const auto& generator = GeneratorRegister<FrameGraphStateGenerator, FrameGraphNode>::Get(frameGraphNode);
    if(generator)
    {
        pipeline.add([&, generator](Pipeline::Stage& next) {
            (*generator)(next, frameContext, frameGraphNodeContext, entity);
        });
    }
}

}

static const int Generator_FrameGraphNodeA = GeneratorRegister<FrameGraphStateGenerator, FrameGraphNode>::Add<FrameGraphNodeA>([](const FrameGraphNodeA::Ptr& frameGraphNodeA) {
    return [frameGraphNodeA](Pipeline::Stage& next, FrameContext&, const FrameGraphNodeContext&, const Entity::Ptr&)
    {
        std::cout << ">>>>> Binding " << frameGraphNodeA->className() << std::endl;

        next();

        std::cout << ">>>>> Unbinding " << frameGraphNodeA->className() << std::endl;
    };
});

static const int Generator_FrameGraphNodeB = GeneratorRegister<FrameGraphStateGenerator, FrameGraphNode>::Add<FrameGraphNodeB>([](const FrameGraphNodeB::Ptr& frameGraphNodeB) {
    return [frameGraphNodeB](Pipeline::Stage& next, FrameContext&, const FrameGraphNodeContext&, const Entity::Ptr&)
    {
        std::cout << ">>>>> Binding " << frameGraphNodeB->className() << std::endl;

        next();

        std::cout << ">>>>> Unbinding " << frameGraphNodeB->className() << std::endl;
    };
});

static const int Generator_FrameGraphNodeC = GeneratorRegister<FrameGraphGenerator, FrameGraphNode>::Add<FrameGraphNodeC>([](const FrameGraphNodeC::Ptr& frameGraphNodeC) {
    return [frameGraphNodeC](FrameContext& frameContext, const FrameGraphNodeContext& frameGraphNodeContext, const Entity::Ptr& entity)
    {
        std::cout << "+++++ " << frameGraphNodeC->className() << " implementation" << std::endl;

        Pipeline pipeline;

        AddLastToPipeline<FrameGraphNodeA>(pipeline, frameContext, frameGraphNodeContext, entity);
        AddLastToPipeline<FrameGraphNodeB>(pipeline, frameContext, frameGraphNodeContext, entity);

        pipeline.execute([&]() {
            Propagator<Entity>().propagate<EntityContext>(entity, [&frameContext, &frameGraphNodeContext, &frameGraphNodeC](const EntityContext& parentEntityContext, const Entity::Ptr& entity) {
                EntityContext entityContext(parentEntityContext, entity);

                for(const Component::Ptr& component : entity->components())
                {
                    const auto& generator = GeneratorRegister<ComponentGenerator, Component, FrameGraphNodeC>::Get(component, frameGraphNodeC);
                    if(generator)
                    {
                        (*generator)(frameContext, frameGraphNodeContext, entityContext);
                    }
                }

                return entityContext;
            });
        });

        std::cout << "----- " << frameGraphNodeC->className() << " implementation" << std::endl;
    };
});

int main() try
{
    std::cout << std::endl;

// graph description

    auto f_root = FrameGraphNode::CreateRoot("f_root");
    {
        auto f_a = f_root->createChild<FrameGraphNodeA>("f_a");
        {
            auto f_aa = f_a->createChild<FrameGraphNodeB>("f_aa");
            {
                auto f_aaa = f_aa->createChild<FrameGraphNodeC>("f_aaa");
            }
        }
        auto f_b = f_root->createChild<FrameGraphNodeB>("f_b");
        auto f_c = f_root->createChild<FrameGraphNodeA>("f_c");
        {
            auto f_ca = f_c->createChild<FrameGraphNodeC>("f_ca");
        }
    }

// graph print

    Visitor<FrameGraphNode>().visit(f_root, [](FrameGraphNode::Ptr frameGraphNode) {
        std::stringstream indentation;
        for(FrameGraphNode::Ptr parent = frameGraphNode->parentFrameGraphNode(); parent; parent = parent->parentFrameGraphNode())
        {
            indentation << "| ";
        }

        std::cout << indentation.str() << frameGraphNode->className() << " - " << frameGraphNode->name() << std::endl;
    });

// graph description

    auto e_root = Entity::CreateRoot("e_root");
    {
        auto e_a = e_root->createChild("e_a");
        {
            auto c_a = e_root->createComponent<ComponentA>("c_aa");
            auto c_b = e_root->createComponent<ComponentB>("c_ab");
        }

        auto e_b = e_root->createChild("e_b");
        {
            auto c_a = e_root->createComponent<ComponentA>("c_ba");
        }
    }

    std::cout << std::endl;

// graph generate

    FrameContext frameContext;
    Propagator<FrameGraphNode>().propagate<FrameGraphNodeContext>(f_root, [&frameContext, &e_root](const FrameGraphNodeContext& parentFrameGraphNodeContext, const FrameGraphNode::Ptr& frameGraphNode) {
        FrameGraphNodeContext frameGraphNodeContext(parentFrameGraphNodeContext, frameGraphNode);

        const auto& generator = GeneratorRegister<FrameGraphGenerator, FrameGraphNode>::Get(frameGraphNode);
        if(generator)
        {
            (*generator)(frameContext, frameGraphNodeContext, e_root);
        }

        return frameGraphNodeContext;
    });

    std::cout << frameContext.result() << std::endl;

    return 0;
}
catch(const std::exception& e)
{
    std::cerr << e.what() << std::endl;
}
