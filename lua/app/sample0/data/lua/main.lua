console = {

    log = function(...)
        print('lua:', ...)
    end,

    debug = function(...)
        print('lua:', '[DEBUG]', ...)
    end,

    warning = function(...)
        print('lua:', '[WARN]', ...)
    end,

    error = function(...)
        print('lua:', '[ERROR]', ...)
    end,

    assert = function(assertion, ...)
        if not assertion then
            print('lua:', '[ASSERT]', ...)
        end
    end

}

console.log("STARTING LUA", XLuaEngine.name)

o = XItem:create()
o.name = "o"

console.log("o", o, o.name)

--console.assert(1 == 2, "1 != 2")

--do return end
console.log("callable0", o:callable0(5.2, 7, 2))
console.log("callable1", o:callable1(5.7, 1.1))
console.log("callable2", o:callable2())
console.log("callable3", o:callable3())
console.log("callable4", o:callable4())

console.log("callable5", o:callable5(o))
console.log("callable6", o:callable6())
console.log("callable7", o:callable7(o))
--console.log("callable8")
--o:callable8() -- raise an exception hence exit application

t, o = nil

collectgarbage()

console.log("EventLoop", XEventLoop.name)

XCounter = { count = 0 }

function XCounter:create()
    o = {}
    setmetatable(o, self)
    self.__index = self
    return o
end

function XCounter:update()
    self.count = self.count + 1
    console.log("update", self.count)
    if 100 == self.count then
        XEventLoop.quit()
    end
end

p = XCounter:create()
p:update()
p:update()
t = (function() p:update() end)

t()
t()
t()

--XEventLoop.onUpdate(update)

console.log("EXITING LUA")
