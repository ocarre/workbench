#include <lua_engine/xluaengine.h>

#include <iostream>
#include <queue>

class XLog
{
public:
    XLog(const std::string &flag) : myFlag(flag),
                                    myStream()
    {
    }

    ~XLog()
    {
        std::cout << myFlag << myStream.str() << NormalFlag << std::endl;
    }

    template <class T>
    XLog &operator<<(const T &message)
    {
        myStream << message;
        return *this;
    }

protected:
    static std::string NormalFlag;

private:
    std::string myFlag;
    std::stringstream myStream;
};
std::string XLog::NormalFlag = "\033[0m";

#define xInfo() XLog("\033[1;34m")
#define xDebug() XLog("\033[1;32m")
#define xWarning() XLog("\033[1;33m")
#define xError() XLog("\033[1;31m")
#define xFatal() XLog("\033[1;35m")

enum class XEventType
{
    XUpdate = 0,
};

class XEvent
{
public:
    using XEventFunctor = std::function<void()>;

    XEventType type() const { return myType; }

private:
    XEventType myType;
    XEventFunctor myFunctor;
};

class XEventLoop
{
    XBEGIN_BASE_DECLARATION(XEventLoop)

    XADD_FUNCTION(post)
    XADD_FUNCTION(quit)

    XEND_DECLARATION

public:
    XEventLoop() : myEvents(),
                   myFinished(false)
    {
    }

    void exec()
    {
        myFinished = false;

        while (!myFinished)
        {
            if (!myEvents.empty())
            {
                const XEvent &event = myEvents.front();

                process(event);

                myEvents.pop();
            }
        }

        myFinished = false;
    }

    void post(const XEvent &event)
    {
    }

    void quit()
    {
        myFinished = true;
    }

protected:
    void process(const XEvent &event)
    {
        switch (event.type())
        {
        case XEventType::XUpdate:
            quit();
        }
    }

private:
    std::queue<XEvent> myEvents;
    bool myFinished;
};

class XItem
{
    XBEGIN_BASE_DECLARATION(XItem)

    XADD_PROPERTY_RW(std::string, name, name, setName)
    XADD_PROPERTY_RW(XItem *, a, a, setA)
    XADD_PROPERTY_RW(int, b, b, setB)

    XADD_FUNCTION(callable0)
    XADD_FUNCTION(callable1)
    XADD_FUNCTION(callable2)
    XADD_FUNCTION(callable3)
    XADD_FUNCTION(callable4)
    XADD_FUNCTION(callable5)
    XADD_FUNCTION(callable6)
    XADD_FUNCTION(callable7)
    XADD_FUNCTION(callable8)

    XEND_DECLARATION

public:
    XItem() : myName("NoName"),
              myA(nullptr),
              myB(87)
    {
        xInfo() << "cpp:\t"
                << "XItem::ctor() - " << this;
    }

    ~XItem()
    {
        xInfo() << "cpp:\t"
                << "XItem::dtor() - " << this;
    }

public:
    const std::string &name() const { return myName; }
    void setName(const std::string &name) { myName = name; }

    XItem *const &a() const { return myA; }
    void setA(XItem *const &a) { myA = a; }

    const int &b() const { return myB; }
    void setB(const int &b) { myB = b; }

    int callable0(int x, int y, int z)
    {
        return x + y + z;
    }

    double callable1(double x, double y)
    {
        return x * y;
    }

    XItem *callable2()
    {
        return this;
    }

    std::string callable3() const
    {
        return "cpp";
    }

    void callable4()
    {
    }

    XItem *callable5(XItem *other)
    {
        xInfo() << "cpp:\t"
                << "callable5\t"
                << "ptr: " << other;

        return other;
    }

    XItem *callable6()
    {
        return new XItem(); // will leak
    }

    XItem *callable7(XItem *other)
    {
        xInfo() << "cpp:\t"
                << "callable7\t"
                << "ptr: " << other;

        return other;
    }

    void callable8()
    {
        throw std::runtime_error("Correctly catched exception");
    }

private:
    std::string myName;
    XItem *myA;
    int myB;
};

int main(int, char *[])
{
    xInfo() << "cpp:\t"
            << "App launched";
    {
        XLuaEngine engine;
        engine.addImportPath("data/lua");

        engine.registerClass<XItem>();

        XEventLoop eventLoop;

        engine.registerSingletonClass(&eventLoop);
        engine.require("main");

        // eventLoop.post(&Update);
        // eventLoop.exec();
    }
    xInfo() << "cpp:\t"
            << "App terminated"
            << " " << sizeof(int) << " " << sizeof(std::unordered_map<XScriptEngine *, XBaseInstance *>);

    return 0;
}
