#ifndef XFUNCTION_H
#define XFUNCTION_H

#include "xvariant.h"

#include <vector>
#include <map>
#include <functional>
#include <utility>

template<class FunctionType>
class XFunctionInfo;

template<class ClassType, class ReturnType, class ... ArgumentTypes>
class XFunctionInfo<ReturnType(ClassType::*)(ArgumentTypes...)>
{
public:
    using Function = std::function<ReturnType(ClassType*, ArgumentTypes...)>;
    using Class = ClassType;
    using Return = ReturnType;
    using PackedArguments = std::tuple<ArgumentTypes...>;

};

template<class ClassType, class ReturnType, class ... ArgumentTypes>
class XFunctionInfo<ReturnType(ClassType::*)(ArgumentTypes...) const>
{
public:
    using Function = std::function<ReturnType(const ClassType*, ArgumentTypes...)>;
    using Class = ClassType;
    using Return = ReturnType;
    using PackedArguments = std::tuple<ArgumentTypes...>;

};

template<class Function>
inline XType BuildReturnType_Helper()
{
    return XVariant::Type<typename XFunctionInfo<Function>::Return>();
}

template<class Arguments, size_t ... IndexSequence>
inline std::vector<XType> BuildArgumentTypes_Helper(std::index_sequence<IndexSequence...>)
{
    return { XVariant::Type<typename std::tuple_element<IndexSequence, Arguments>::type>()... };
}

template<class Function>
inline std::vector<XType> BuildArgumentTypes_Helper()
{
    using PackedArguments = typename XFunctionInfo<Function>::PackedArguments;
    static constexpr auto ArgumentIndices = std::make_index_sequence<std::tuple_size<PackedArguments>::value>();

    return BuildArgumentTypes_Helper<PackedArguments>(ArgumentIndices);
}

class XBaseFunction
{
protected:
    XBaseFunction(const std::string& name, XType returnType, const std::vector<XType>& parameterTypes) :
        myName(name),
        myReturnType(returnType),
        myParameterTypes(parameterTypes)
    {

    }

public:
    virtual ~XBaseFunction();

    virtual XVariant call(void* object, const std::vector<XVariant>& parameters) const = 0;

    const std::string& name() const { return myName;}
    XType returnType() const { return myReturnType; }
    const std::vector<XType>& parameterTypes() const { return myParameterTypes; }

private:
    std::string         myName;
    XType               myReturnType;
    std::vector<XType>  myParameterTypes;

};

template<class FunctionType>
class XFunction : public XBaseFunction
{
    using FunctionInfo = XFunctionInfo<FunctionType>;
    using Function = typename FunctionInfo::Function;
    using Class = typename FunctionInfo::Class;
    using Return = typename FunctionInfo::Return;
    using PackedArguments = typename FunctionInfo::PackedArguments;
    static constexpr size_t Arity = std::tuple_size<PackedArguments>::value;
    static constexpr auto ArgumentIndices = std::make_index_sequence<Arity>();

public:
    XFunction(const FunctionType& function, const std::string& name) : XBaseFunction(name, BuildReturnType_Helper<FunctionType>(), BuildArgumentTypes_Helper<FunctionType>()),
        myFunction(function)
    {

    }

    virtual XVariant call(void* object, const std::vector<XVariant>& parameters) const
    {
        XVariant result = XVariant();

        if(Arity != parameters.size())
        {
            std::cout << "[ERROR]: XFunction::call - bad number of arguments, " << Arity << " required, " << parameters.size() << " given" << std::endl;
            return result;
        }

        return onCall(static_cast<Class*>(object), parameters);
    }

private:
    template<size_t ... IndexSequence>
    Return onCallImpl(Class* object, const std::vector<XVariant>& parameters, std::index_sequence<IndexSequence...>) const
    {
        return myFunction(object, parameters[IndexSequence].template value<typename std::tuple_element<IndexSequence, PackedArguments>::type>()...);
    }

    template<class ReturnType = Return>
    typename std::enable_if<!std::is_void<ReturnType>::value, XVariant>::type onCall(Class* object, const std::vector<XVariant>& parameters) const
    {
        return XVariant(onCallImpl(object, parameters, ArgumentIndices));
    }

    template<class ReturnType = Return>
    typename std::enable_if<std::is_void<ReturnType>::value, XVariant>::type onCall(Class* object, const std::vector<XVariant>& parameters) const
    {
        onCallImpl(object, parameters, ArgumentIndices);

        return XVariant();
    }

private:
    Function myFunction;

};

using XFunctions = std::map<std::string, XBaseFunction*>;


#endif
