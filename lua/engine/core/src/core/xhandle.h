#ifndef XHANDLE_H
#define XHANDLE_H

#include <string>
#include <iostream>
#include <vector>
#include <list>
#include <map>

/// \brief generic instance handle that should specialized accordingly to the script engine implementation
class XBaseHandle
{
public:
    virtual ~XBaseHandle() {}

    virtual std::unique_ptr<XBaseHandle> clone() const = 0;

};

template<class T>
class XHandle : public XBaseHandle
{
public:
    XHandle(const T& value);

    std::unique_ptr<XBaseHandle> clone() const override;

    T& value();
    const T& value() const;

private:
    T myValue;

};

template<class T>
inline XHandle<T>::XHandle(const T& value) :
    myValue(value)
{

}

template<class T>
inline std::unique_ptr<XBaseHandle> XHandle<T>::clone() const
{
    return std::unique_ptr<XBaseHandle>(new XHandle<T>(myValue));
}

template<class T>
inline T& XHandle<T>::value()
{
    return const_cast<T&>((static_cast<const XHandle<T>*>(this))->value());
}

template<class T>
inline const T& XHandle<T>::value() const
{
    return myValue;
}

#endif
