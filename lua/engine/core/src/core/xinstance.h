#ifndef XINSTANCE_H
#define XINSTANCE_H

#include "xvariant.h"
#include "xfunction.h"
#include "xproperty.h"
#include "xhandle.h"
#include "xmeta.h"

#include <map>
#include <vector>
#include <string>

class XBaseInstance
{
protected:
    XBaseInstance(XBaseMeta &meta);

public:
    virtual ~XBaseInstance();

    XBaseMeta &meta() const { return myMeta; }

    virtual void *nativePointer() const = 0;

    template <class T>
    T scriptReference() const { return static_cast<XHandle<T> *>(myHandle)->value(); }

    template <class T>
    void setScriptReference(const T &reference) { myHandle = new XHandle<T>(reference); }

private:
    XBaseMeta &myMeta;
    XBaseHandle *myHandle;
};

template <class T>
class XInstance : public XBaseInstance
{
public:
    XInstance() : XInstance(new T()) {}
    XInstance(T *object) : XBaseInstance(T::XMeta::instance()),
                           myInstance(object)
    {
    }

public:
    void *nativePointer() const override { return myInstance.get(); }

private:
    std::unique_ptr<T> myInstance;
};

#endif
