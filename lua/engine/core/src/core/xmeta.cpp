#include "xmeta.h"
#include "xinstance.h"

#include <iostream>

XBaseMeta::~XBaseMeta()
{

}

void XBaseMeta::destroy(XBaseInstance* instance)
{
    delete instance;
}

bool XBaseMeta::propertyExists(const std::string& name) const
{
    return properties().end() != properties().find(name);
}

std::vector<std::string> XBaseMeta::propertyNames() const
{
    std::vector<std::string> propertyNames;

    XProperties properties = XBaseMeta::properties();
    for(auto iterator = properties.begin(); iterator != properties.end(); ++iterator)
        propertyNames.push_back(iterator->first);

    return propertyNames;
}

bool XBaseMeta::functionExists(const std::string& name) const
{
    return functions().end() != functions().find(name);
}

std::vector<std::string> XBaseMeta::functionNames() const
{
    std::vector<std::string> functionNames;

    XFunctions functions = this->functions();
    for(auto iterator = functions.begin(); iterator != functions.end(); ++iterator)
        functionNames.push_back(iterator->first);

    return functionNames;
}

XBaseProperty& XBaseMeta::property(const std::string& name)
{
    return const_cast<XBaseProperty&>(((const XBaseMeta*) this)->property(name));
}

const XBaseProperty& XBaseMeta::property(const std::string& name) const
{
    return *properties().at(name);
}

XBaseFunction& XBaseMeta::function(const std::string& name)
{
    return const_cast<XBaseFunction&>(((const XBaseMeta*) this)->function(name));
}

const XBaseFunction& XBaseMeta::function(const std::string& name) const
{
    return *functions().at(name);
}

void XBaseMeta::addProperty(const std::string& name, XBaseProperty* property)
{
    myProperties.emplace(name, property);
}

void XBaseMeta::addFunction(const std::string& name, XBaseFunction* function)
{
    myFunctions.emplace(name, function);
}

void XBaseMeta::addProperties(const XProperties& properties)
{
    myProperties.insert(properties.begin(), properties.end());
}

void XBaseMeta::addFunctions(const XFunctions& functions)
{
    myFunctions.insert(functions.begin(), functions.end());
}
