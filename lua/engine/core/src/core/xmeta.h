#ifndef XMETA_H
#define XMETA_H

#include "xvariant.h"
#include "xfunction.h"
#include "xproperty.h"
#include "xhandle.h"

#include <map>
#include <vector>
#include <string>

class XBaseInstance;

/// \brief contains information about a class definition (how to construct / destruct, properties and callables)
class XBaseMeta
{
protected:
    XBaseMeta(const std::string &name) : myName(name) {}
    virtual ~XBaseMeta();

public:
    virtual XBaseInstance *create() const = 0;
    virtual XBaseInstance *wrap(void *ptr) const = 0;
    virtual void destroy(XBaseInstance *instance);

    const std::string &name() const { return myName; }
    const XProperties &properties() const { return myProperties; }
    const XFunctions &functions() const { return myFunctions; }

    bool propertyExists(const std::string &name) const;
    std::vector<std::string> propertyNames() const;

    bool functionExists(const std::string &name) const;
    std::vector<std::string> functionNames() const;

    XBaseProperty &property(const std::string &name);
    const XBaseProperty &property(const std::string &name) const;

    XBaseFunction &function(const std::string &name);
    const XBaseFunction &function(const std::string &name) const;

protected:
    void addProperty(const std::string &name, XBaseProperty *property);
    void addFunction(const std::string &name, XBaseFunction *function);

    void addProperties(const XProperties &properties);
    void addFunctions(const XFunctions &functions);

private:
    std::string myName;
    XProperties myProperties;
    XFunctions myFunctions;
};

template <class T>
class XInstanceWrapper
{
public:
    XInstanceWrapper() : myObject(nullptr)
    {
    }

    void set(T *object)
    {
        myObject = object;
    }

    void clear()
    {
        myObject = nullptr;
    }

private:
    T *myObject;
};

#define XBEGIN_COMMON_DECLARATION(owner)                            \
protected:                                                          \
    XInstanceWrapper<owner> instanceWrapper;                        \
                                                                    \
public:                                                             \
    class XMeta : public XBaseMeta                                  \
    {                                                               \
    private:                                                        \
        XMeta(const std::string &name) : XBaseMeta(name) {}         \
        XBaseInstance *create() const override                      \
        {                                                           \
            XBaseInstance *instance = wrap(new owner());            \
            return instance;                                        \
        }                                                           \
                                                                    \
        XBaseInstance *wrap(void *ptr) const override               \
        {                                                           \
            return new XInstance<owner>(static_cast<owner *>(ptr)); \
        }                                                           \
                                                                    \
    public:                                                         \
        friend class owner;                                         \
        typedef owner XOwner;                                       \
        static XMeta &instance()                                    \
        {                                                           \
            static XMeta instance(#owner);                          \
            static bool init = false;                               \
            if (!init)                                              \
            {

#define XBEGIN_BASE_DECLARATION(owner) \
protected:                             \
    XBEGIN_COMMON_DECLARATION(owner)

#define XBEGIN_INHERIT_DECLARATION(owner, parent)                   \
    XBEGIN_COMMON_DECLARATION(owner)                                \
    instance.addProperties(parent::XMeta::instance().properties()); \
    instance.addFunctions(parent::XMeta::instance().functions());

#define XADD_PROPERTY_MEMBER(type, name, member)                                                                              \
    instance.addProperty(#name, new XProperty<type, XOwner, const type &(XOwner::*)() const, void (XOwner::*)(const type &)>( \
                                    #name, [](const XOwner *object) -> const type & { return object->member; }, [](XOwner *object, const type &value) { object->member = value; }));

#define XADD_PROPERTY_RO(type, name, get) \
    instance.addProperty(#name, = new XProperty<type, XOwner, decltype(&XOwner::get)>(#name, &XOwner::get));

#define XADD_PROPERTY_RW(type, name, get, set) \
    instance.addProperty(#name, new XProperty<type, XOwner, decltype(&XOwner::get), decltype(&XOwner::set)>(#name, &XOwner::get, &XOwner::set));

#define XADD_FUNCTION(function) \
    instance.addFunction(#function, new XFunction<decltype(&XOwner::function)>(&XOwner::function, #function));

#define XEND_DECLARATION \
    init = true;         \
    }                    \
    return instance;     \
    }                    \
    }                    \
    ;                    \
                         \
private:

#endif
