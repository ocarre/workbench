#ifndef XOS_H
#define XOS_H

#ifdef WIN32
    #define XLIB_IMPORT __declspec(dllimport)
    #define XLIB_EXPORT __declspec(dllexport)
#else
    #define XLIB_IMPORT
    #define XLIB_EXPORT
#endif

/*
 * Usage (only for dynamic library)
 *

#include "xos.h"

#ifdef XMYLIB_BUILD
    #define XMYLIB_API XLIB_EXPORT
#else
    #define XMYLIB_API XLIB_IMPORT
#endif

*/

#endif
