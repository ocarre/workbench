#ifndef XPROPERTY_H
#define XPROPERTY_H

#include "xvariant.h"

#include <iostream>
#include <map>
#include <string>
#include <functional>
#include <utility>

class XBaseProperty
{
protected:
    XBaseProperty(const std::string& name, XType type) :
        myName(name),
        myType(type)
    {

    }

public:
    virtual ~XBaseProperty();

    virtual XVariant getVariant(const void* object) const           = 0;
    virtual void setVariant(void* object, const XVariant& variant)  = 0;

    const std::string& name() const {return myName;}
    XType type() const { return myType; }

private:
    std::string     myName;
    XType myType;

};

template<class Type, class Class, class Get = void, class Set = void>
class XProperty;

template<class Type, class Class, class GetReturnType, class SetArgumentType>
class XProperty<Type, Class, GetReturnType(Class::*)() const, void(Class::*)(SetArgumentType)> : public XBaseProperty
{
public:
    typedef GetReturnType(Class::*GetType)() const;
    typedef void(Class::*SetType)(SetArgumentType);

    // default read / write
    XProperty(const std::string& name) : XBaseProperty(name, XVariant::Type<Type>()),
        myGetter(),
        mySetter()
    {

    }

    // read / write
    XProperty(const std::string& name, std::function<GetReturnType(const Class*)> getter, std::function<void(Class*, SetArgumentType)> setter) : XBaseProperty(name, XVariant::Type<Type>()),
        myGetter(getter),
        mySetter(setter)
    {

    }

    virtual XVariant getVariant(const void* object) const
    {
        if(myGetter)
            return myGetter(static_cast<const Class*>(object));
        else
            std::cout << "[WARN] " << "cannot get the property '" << name() << "' because it is not readable" << std::endl;

        return XVariant();
    }

    virtual void setVariant(void* object, const XVariant& variant)
    {
        if(mySetter)
            mySetter(static_cast<Class*>(object), variant.value<SetArgumentType>());
        else
            std::cout << "[WARN] " << "cannot set the property '" << name() << "' because it is not writable" << std::endl;
    }

private:
    std::function<GetReturnType(const Class*)>      myGetter;
    std::function<void(Class*, SetArgumentType)>    mySetter;

};

typedef std::map<std::string, XBaseProperty*> XProperties;

#endif
