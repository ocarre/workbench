#include "xscriptengine.h"

#include <iostream>

XScriptEngine::XScriptEngine() :
    myClasses(),
    myInstances(),
    myVerbose(false)
{

}

XScriptEngine::~XScriptEngine()
{

}

XBaseMeta* XScriptEngine::meta(const std::string& name) const
{
    for(auto i : myClasses)
        std::cout << ">" << i.first << std::endl;

    auto iterator = myClasses.find(name);
    if(myClasses.end() != iterator)
        return iterator->second;

    return nullptr;
}
