#ifndef XSCRIPTENGINE_H
#define XSCRIPTENGINE_H

#include "xproperty.h"
#include "xinstance.h"

#include <functional>
#include <unordered_map>
#include <map>
#include <vector>
#include <string>

#include <iostream>
#include <sstream>

class XScriptEngine
{
public:
    XScriptEngine();
    virtual ~XScriptEngine();

    void setVerbose(bool verbose) { myVerbose = verbose; }

    template<class T>
    void registerClass();

    template<class T>
    void registerSingletonClass(T* ptr);

    virtual void addImportPath(const std::string& path) = 0;
    virtual void require(const std::string& filename) = 0;

    static constexpr const char* ClassName() { return "XScriptEngine"; }
    XBaseMeta* meta(const std::string& name) const;

protected:
    virtual void registerClassByName(const std::string& name) = 0;
    virtual void registerSingletonClassByName(const std::string& name, void* ptr) = 0;

protected:
    std::map<std::string, XBaseMeta*>           myClasses;
    std::unordered_map<void*, XBaseInstance*>   myInstances;
    bool                                        myVerbose;

};

template<class T>
inline void XScriptEngine::registerClass()
{
    XBaseMeta& meta = T::XMeta::instance();
    myClasses[meta.name()] = &meta;

    std::cout << "[INFO] Registering type: " << meta.name() << std::endl;

    registerClassByName(meta.name());
}

template<class T>
inline void XScriptEngine::registerSingletonClass(T* ptr)
{
    XBaseMeta& meta = T::XMeta::instance();
    myClasses[meta.name()] = &meta;

    std::cout << "[INFO] Registering singleton type: " << meta.name() << std::endl;

    registerSingletonClassByName(meta.name(), ptr);
}

#endif
