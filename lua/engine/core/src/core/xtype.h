#ifndef XTYPE_H
#define XTYPE_H

#include <iostream>

enum class XType
{
    VOID = 0,
    OBJECT,
    BOOL,
    INT,
    FLOAT,
    DOUBLE,
    STRING,
    VECTOR,
    LIST,
    MAP

};

inline std::ostream& operator<<(std::ostream& os, XType type)
{
    switch(type)
    {
    case XType::VOID:       os << "VOID";        break;
    case XType::OBJECT:     os << "OBJECT";      break;
    case XType::BOOL:       os << "BOOL";        break;
    case XType::INT:        os << "INT";         break;
    case XType::FLOAT:      os << "FLOAT";       break;
    case XType::DOUBLE:     os << "DOUBLE";      break;
    case XType::STRING:     os << "STRING";      break;
    case XType::VECTOR:     os << "VECTOR";      break;
    case XType::LIST:       os << "LIST";        break;
    case XType::MAP:        os << "MAP";         break;
    }

    return os;
}

#endif
