#include "xvariant.h"
#include "xinstance.h"

XBaseVariantValueWrapper::XBaseVariantValueWrapper() :
    myMeta(nullptr)
{

}

XBaseVariantValueWrapper::~XBaseVariantValueWrapper()
{

}

template<>
void XVariantValueWrapper<std::string>::print(std::ostream& stream) const
{
    stream << "\"" << myValue << "\"";
}

template<>
void XVariantValueWrapper<std::vector<XVariant>>::print(std::ostream& stream) const
{
    for(auto iterator = myValue.begin(); iterator != myValue.end(); ++iterator)
    {
        const XVariant& value = *iterator;

        if(myValue.begin() != iterator)
            stream << ", ";

        stream << value;
    }
}

template<>
void XVariantValueWrapper<std::list<XVariant>>::print(std::ostream& stream) const
{
    for(auto iterator = myValue.begin(); iterator != myValue.end(); ++iterator)
    {
        const XVariant& value = *iterator;

        if(myValue.begin() != iterator)
            stream << ", ";

        stream << value;
    }
}

template<>
void XVariantValueWrapper<std::map<std::string, XVariant>>::print(std::ostream& stream) const
{
    for(auto iterator = myValue.begin(); iterator != myValue.end(); ++iterator)
    {
        const std::string& name = iterator->first;
        const XVariant& value = iterator->second;

        if(myValue.begin() != iterator)
            stream << ", ";

        stream << "\"" << name << "\"" << "=" << value;
    }
}

XVariant::XVariant() :
    myValueWrapper(),
    myType(Type<void>()),
    myMeta(nullptr)
{

}

XVariant::XVariant(const XVariant& variant) :
    myValueWrapper(),
    myType(Type<void>()),
    myMeta(nullptr)
{
    this->operator=(variant);
}

XVariant::XVariant(XVariant&& variant) :
    myValueWrapper(),
    myType(Type<void>()),
    myMeta(nullptr)
{
    this->operator=(std::move(variant));
}

XVariant::XVariant(void* ptr, XBaseMeta* meta) :
    myValueWrapper(new XVariantValueWrapper<void*>(ptr)),
    myType(XType::OBJECT),
    myMeta(meta)
{

}

XVariant& XVariant::operator=(const XVariant& variant)
{
    myType = variant.myType;
    myValueWrapper = variant.myValueWrapper ? variant.myValueWrapper->clone() : nullptr;
    myMeta = variant.myMeta;

    return *this;
}

XVariant& XVariant::operator=(XVariant&& variant)
{
    myValueWrapper.reset(variant.myValueWrapper.release());
    myType = (std::exchange(variant.myType, XType::VOID));
    myMeta = (std::exchange(variant.myMeta, nullptr));

    return *this;
}
