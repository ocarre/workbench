#ifndef XVARIANT_H
#define XVARIANT_H

#include "xtype.h"

#include <string>
#include <iostream>
#include <vector>
#include <list>
#include <map>

class XBaseMeta;

template<class T>
class has_xmeta
{
    template <typename C> static constexpr std::true_type check(typename C::XMeta*);
    template <typename C> static constexpr std::false_type check(...);

public:
    enum { value = std::is_same<decltype(check<T>(nullptr)), std::true_type>::value };

};

class xmeta_helper
{
public:
    template<class T>
    static constexpr auto deduce() -> typename std::enable_if<has_xmeta<typename std::remove_pointer<T>::type>::value, XBaseMeta*>::type
    {
        return &std::remove_pointer<T>::type::XMeta::instance();
    }

    template<class T>
    static constexpr auto deduce() -> typename std::enable_if<!has_xmeta<typename std::remove_pointer<T>::type>::value, XBaseMeta*>::type
    {
        return nullptr;
    }

};

class XBaseVariantValueWrapper
{
protected:
    XBaseVariantValueWrapper();

public:
    virtual ~XBaseVariantValueWrapper();

    virtual std::unique_ptr<XBaseVariantValueWrapper> clone() const = 0;

    virtual void print(std::ostream& stream = std::cout) const = 0;

private:
    XBaseMeta* myMeta;

};

inline std::ostream& operator << (std::ostream& stream, const XBaseVariantValueWrapper& variantValueWrapper)
{
    variantValueWrapper.print(stream);

    return stream;
}

template<class T>
class XVariantValueWrapper : public XBaseVariantValueWrapper
{
public:
    XVariantValueWrapper(const T& value);

    virtual std::unique_ptr<XBaseVariantValueWrapper> clone() const;

    virtual void print(std::ostream& stream = std::cout) const;

    T& value();
    const T& value() const;

private:
    T myValue;

};

template<class T>
inline XVariantValueWrapper<T>::XVariantValueWrapper(const T& value) :
    myValue(value)
{

}

template<class T>
inline std::unique_ptr<XBaseVariantValueWrapper> XVariantValueWrapper<T>::clone() const
{
    std::cout << "x" << std::endl;
    auto test = new XVariantValueWrapper<T>(myValue);
    std::cout << "y" << std::endl;
    return std::unique_ptr<XBaseVariantValueWrapper>(test);
}

template<class T>
inline void XVariantValueWrapper<T>::print(std::ostream& stream) const
{
    stream << myValue;
}

template<class T>
inline T& XVariantValueWrapper<T>::value()
{
    return const_cast<T&>((static_cast<const XVariantValueWrapper<T>*>(this))->value());
}

template<class T>
inline const T& XVariantValueWrapper<T>::value() const
{
    return myValue;
}

class XBaseInstance;

class XVariant
{
public:
    XVariant();
    XVariant(const XVariant& variant);
    XVariant(XVariant&& variant);
    XVariant(void* ptr, XBaseMeta* meta);

    template<class T>
    XVariant(const T& value);

//    template<class T>
//    XVariant(T* object);

    bool isValid() const {return XType::VOID != myType && myValueWrapper;}

    template<class T>
    T& value();

    template<class T>
    const T& value() const;

    XType type() const { return myType; }

    XBaseMeta* meta() const { return myMeta; }

    template<class T>
    static auto Type() -> typename std::enable_if<!std::is_pointer<T>::value, XType>::type;

    template<class T>
    static auto Type() -> typename std::enable_if<std::is_pointer<T>::value, XType>::type;

    operator bool() const
    {
        return XType::VOID != myType;
    }

    XVariant& operator=(const XVariant& variant);
    XVariant& operator=(XVariant&& variant);

    friend std::ostream& operator << (std::ostream& stream, const XVariant& variant)
    {
        if(XType::VOID == variant.myType || 0 == variant.myValueWrapper)
            return stream << "XVariant(" << variant.myType << ")";

        return stream << "XVariant(" << variant.myType << ", " << *variant.myValueWrapper << ")";
    }

private:
    std::unique_ptr<XBaseVariantValueWrapper>   myValueWrapper;
    XType                                       myType;
    XBaseMeta*                                  myMeta;

};

template<class T>
inline XVariant::XVariant(const T& value) :
    myValueWrapper(new XVariantValueWrapper<T>(value)),
    myType(Type<T>()),
    myMeta(XType::VOID != myType ? xmeta_helper::deduce<T>() : nullptr)
{
//    std::cout << "meta: " << myType << " - " << myMeta << " - " << typeid(T).name() << std::endl;
}

//template<class T>
//inline XVariant::XVariant(T* object) :
//    myValueWrapper(new XVariantValueWrapper<T*>(object)),
//    myType(Type<T*>()),
//    myMeta(xmeta_helper::deduce<T>())
//{

//}

template<class T>
inline T& XVariant::value()
{
    return const_cast<T&>(((const XVariant*) this)->value<T>());
}

template<class T>
inline const T& XVariant::value() const
{
    switch(myType)
    {
    case XType::VOID:           break;
    case XType::OBJECT:         return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<void*>*>(myValueWrapper.get())->value());
    case XType::BOOL:           return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<bool>*>(myValueWrapper.get())->value());
    case XType::INT:            return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<int>*>(myValueWrapper.get())->value());
    case XType::FLOAT:          return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<float>*>(myValueWrapper.get())->value());
    case XType::DOUBLE:         return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<double>*>(myValueWrapper.get())->value());
    case XType::STRING:         return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<std::string>*>(myValueWrapper.get())->value());
    case XType::VECTOR:         return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<std::vector<XVariant>>*>(myValueWrapper.get())->value());
    case XType::LIST:           return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<std::list<XVariant>>*>(myValueWrapper.get())->value());
    case XType::MAP:            return reinterpret_cast<const T&>(static_cast<const XVariantValueWrapper<std::map<std::string, XVariant>>*>(myValueWrapper.get())->value());
    }

    return static_cast<const XVariantValueWrapper<const T>*>(myValueWrapper.get())->value();
}

template<class T>
inline auto XVariant::Type() -> typename std::enable_if<!std::is_pointer<T>::value, XType>::type
{
    return XType::VOID;
}

template<class T>
inline auto XVariant::Type() -> typename std::enable_if<std::is_pointer<T>::value, XType>::type // special case when T is a pointer type
{
    return XType::OBJECT;
}

template<>
inline XType XVariant::Type<bool>()
{
    return XType::BOOL;
}

template<>
inline XType XVariant::Type<int>()
{
    return XType::INT;
}

template<>
inline XType XVariant::Type<float>()
{
    return XType::FLOAT;
}

template<>
inline XType XVariant::Type<double>()
{
    return XType::DOUBLE;
}

template<>
inline XType XVariant::Type<std::string>()
{
    return XType::STRING;
}

template<>
inline XType XVariant::Type<std::vector<XVariant>>()
{
    return XType::VECTOR;
}

template<>
inline XType XVariant::Type<std::list<XVariant>>()
{
    return XType::LIST;
}

template<>
inline XType XVariant::Type<std::map<std::string, XVariant>>()
{
    return XType::MAP;
}

#endif
