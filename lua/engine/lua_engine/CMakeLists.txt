get_filename_component(CURRENT_SOURCE_DIR_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)
project(${CURRENT_SOURCE_DIR_NAME})

file(GLOB HEADER_FILES src/${PROJECT_NAME}/*.h)
source_group("Header" FILES ${HEADER_FILES})

file(GLOB SOURCE_FILES src/${PROJECT_NAME}/*.cpp)
source_group("Source" FILES ${SOURCE_FILES})

find_package(Lua)
include_directories(${LUA_INCLUDE_DIR})

add_library(${PROJECT_NAME} STATIC ${HEADER_FILES} ${SOURCE_FILES})

target_include_directories(${PROJECT_NAME} PUBLIC "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/src>")
target_link_libraries(${PROJECT_NAME} core ${LUA_LIBRARIES})
