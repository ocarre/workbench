#include "xluaengine.h"

#include <lua.hpp>

#define luaX_printfatal(L, message) (lua_pushglobaltable(L), lua_getfield(L, -1, "print"), lua_remove(L, -2), lua_pushstring(L, (std::stringstream() << "\033[0;35m" << "lua:\t[FATAL]" << luaX_formatlinenumber(L) << message << "\033[0m").str().c_str()), lua_call(L, 1, 0), 0)
#define luaX_printerror(L, message) (lua_pushstring(L, (std::stringstream() << "\033[31m" << "lua:\t[ERROR]" << luaX_formatlinenumber(L) << "\t" << message << "\033[0m").str().c_str()), lua_error(L))
#define luaX_printwarning(L, message) (lua_pushglobaltable(L), lua_getfield(L, -1, "print"), lua_remove(L, -2), lua_pushstring(L, (std::stringstream() << "\033[0;33m" << "lua:\t[WARNING]" << luaX_formatlinenumber(L) << message << "\033[0m").str().c_str()), lua_call(L, 1, 0), 0)
#define luaX_printinfo(L, message) (lua_pushglobaltable(L), lua_getfield(L, -1, "print"), lua_remove(L, -2), lua_pushstring(L, (std::stringstream() << "\033[0;32m" << "lua:\t[INFO]" << luaX_formatlinenumber(L) << message << "\033[0m").str().c_str()), lua_call(L, 1, 0), 0)

static std::string luaX_formatlinenumber(lua_State* L)
{
    std::string result;

    lua_Debug ar;
    if(0 != lua_getstack(L, 1, &ar))
    {
        lua_getinfo(L, "nSl", &ar);

        result = "(" + std::string(ar.short_src) + ":L" + std::to_string(ar.currentline) + ")";
    }

    return "\t" + result;
}

static void StreamNestedExceptionErrorHelper(std::ostream& out, const std::exception& e, std::size_t level)
{
    {
        // log current error
        const std::string prefix(level, ' ');
        out << prefix << e.what();
    }

    try {
        std::rethrow_if_nested(e);
    } catch(const std::exception& e) {
        StreamNestedExceptionErrorHelper(out << '\n', e, level + 1);
    }
}

static void StreamExceptionErrorHelper(std::ostream& out, const std::exception& e)
{
    StreamNestedExceptionErrorHelper(out, e, 0);
}

static std::string FormatExceptionErrorHelper(const std::exception& e)
{
    std::stringstream stream;

    StreamExceptionErrorHelper(stream, e);

    return stream.str();
}

template<class T>
inline typename std::enable_if<std::is_pointer<T>::value, T>::type ExtractPointerHelper(const std::string& key, lua_State* L, int idx = -1)
{
    lua_getfield(L, idx, key.c_str());
    T pointer = static_cast<T>(lua_touserdata(L, -1));
    lua_pop(L, 1);

    return pointer;
}

class XLuaEngine::Implementation
{
public:
    Implementation() : L(luaL_newstate())
    {
        luaL_openlibs(L);
    }

    ~Implementation()
    {
        lua_close(L);
    }

    void addImportPath(const std::string& path)
    {
        addImportPath(L, path);
    }

    void require(const std::string& filename)
    {
        require(L, filename);
    }

    void registerClass(const std::string& name)
    {
        registerClass(L, name);
    }

    void registerSingletonClass(const std::string& name, void* ptr)
    {
        registerSingletonClass(L, name, ptr);
    }

protected:

    static XLuaEngine* engine(lua_State* L)
    {
        lua_getglobal(L, ClassName());
        lua_getfield(L, -1, "ptr");
        XLuaEngine* engine = static_cast<XLuaEngine*>(lua_touserdata(L, -1));
        lua_pop(L, 2);

        return engine;
    }

    static void registerClass(lua_State* L, const std::string& name)
    {
        // proxy
        lua_newtable(L);
        {
            // meta
            lua_newtable(L);
            {
                // data
                lua_newtable(L);
                {
                    lua_pushstring(L, name.c_str());
                    lua_setfield(L, -2, "name");

                    lua_pushcfunction(L, create);
                    lua_setfield(L, -2, "create");
                }
                lua_setfield(L, -2, "__index");

                lua_pushnil(L);
                lua_setfield(L, -2, "__newindex");

                lua_pushstring(L, "metatable access denied");
                lua_setfield(L, -2, "__metatable");
            }
            lua_setmetatable(L, -2);
        }
        lua_setglobal(L, name.c_str());
    }

    static void registerSingletonClass(lua_State* L, const std::string& name, void* ptr)
    {
        // proxy
        lua_newtable(L);
        {
            // meta
            lua_newtable(L);
            {
                // data
                lua_newtable(L);
                {
                    lua_pushlightuserdata(L, ptr);
                    lua_setfield(L, -2, "ptr");

                    lua_pushstring(L, name.c_str());
                    lua_setfield(L, -2, "name");

                    // object list
                    lua_newtable(L);
                    {
                        // meta
                        lua_newtable(L);
                        {
                            lua_pushstring(L, "v");
                            lua_setfield(L, -2, "__mode");

                            lua_pushnil(L);
                            lua_setfield(L, -2, "__newindex");

                            lua_pushstring(L, "metatable access denied");
                            lua_setfield(L, -2, "__metatable");
                        }
                        lua_setmetatable(L, -2);
                    }
                    lua_setfield(L, -2, "objects");
                }

                lua_setfield(L, -2, "__index");

                lua_pushnil(L);
                lua_setfield(L, -2, "__newindex");

                lua_pushstring(L, "metatable access denied");
                lua_setfield(L, -2, "__metatable");
            }
            lua_setmetatable(L, -2);
        }
        lua_setglobal(L, name.c_str());
    }

    static void addImportPath(lua_State* L, const std::string& path)
    {
        luaL_dostring(L, (std::string("package.path = package.path..\";") + path + "/?\"").c_str());
        luaL_dostring(L, (std::string("package.path = package.path..\";") + path + "/?.lua\"").c_str());

        //luaL_dostring(L, (std::string("print('package.path', package.path)")).c_str());
    }

    static void require(lua_State* L, const std::string& filename)
    {
        int err = luaL_dostring(L, (std::string("require \"") + filename + "\"").c_str());
        if(0 != err)
        {
            std::string message = lua_tostring(L, -1);
            luaX_printfatal(L, "during a call to XLuaEngine::require on: " << filename << "\n" << message);
        }
    }

    static int create(lua_State* L)
    {
        XLuaEngine* engine = Implementation::engine(L);

        lua_getfield(L, 1, "name");
        std::string name = lua_tostring(L, -1);
        lua_pop(L, 1);

        XBaseMeta* meta = engine->meta(name);
        if(!meta)
            return luaX_printwarning(L, "error while calling XLuaEngine::create on the non-registered class: " << name);

        return wrap(L, meta->create());
    }

    static int destroy(lua_State* L)
    {
        XBaseInstance* instance = ExtractPointerHelper<XBaseInstance*>("instance", L, lua_upvalueindex(1));

        if(instance)
        {
            // clear meta table
            lua_pushvalue(L, lua_upvalueindex(1));
            {
                lua_pushnil(L);
                lua_setfield(L, -2, "instance");

                lua_pushnil(L);
                lua_setfield(L, -2, "__gc");

                lua_pushvalue(L, -1);
                lua_pushcclosure(L, destroyed_tostring, 1);
                lua_setfield(L, -2, "__tostring");

                //lua_pushlightuserdata(L, ptr);
                //lua_setfield(L, -2, "__bool"); // TODO: check object existence (how ? __bool does not exist)
            }
            lua_pop(L, 1);

            // clear reference
            XLuaEngine* engine = Implementation::engine(L);
            auto iterator = engine->myInstances.find(instance->nativePointer());
            if(engine->myInstances.end() != iterator)
                engine->myInstances.erase(iterator);

            // destroy native object
            XBaseMeta& meta = instance->meta();
            meta.destroy(instance);
        }

        return 0;
    }

    static int wrap(lua_State* L, XBaseInstance* instance)
    {
        // proxy
        lua_newtable(L);
        {
            // meta
            lua_newtable(L);
            {
                lua_pushlightuserdata(L, instance);
                lua_setfield(L, -2, "instance");

                lua_pushlightuserdata(L, &instance->meta());
                lua_setfield(L, -2, "type");

                lua_pushvalue(L, 1);
                lua_setfield(L, -2, "class");

                lua_pushvalue(L, -1);
                lua_pushcclosure(L, destroy, 1);
                lua_setfield(L, -2, "__gc");

                lua_pushvalue(L, -1);
                lua_pushcclosure(L, get, 1);
                lua_setfield(L, -2, "__index");

                lua_pushvalue(L, -1);
                lua_pushcclosure(L, set, 1);
                lua_setfield(L, -2, "__newindex");

                lua_pushvalue(L, -1);
                lua_pushcclosure(L, instance_tostring, 1);
                lua_setfield(L, -2, "__tostring");

                //lua_pushlightuserdata(L, ptr);
                //lua_setfield(L, -2, "__bool"); // TODO: check object existance (how ? __bool does not exist)

                // data
                lua_newtable(L);
                {
                    for(const std::string& functionName : instance->meta().functionNames())
                    {
                        lua_pushvalue(L, -2);
                        lua_pushstring(L, functionName.c_str());
                        lua_pushcclosure(L, call, 2);
                        lua_setfield(L, -2, functionName.c_str());
                    }

                    // destroy
                    {
                        lua_pushvalue(L, -2);
                        lua_pushcclosure(L, destroy, 1);
                        lua_setfield(L, -2, "destroy");
                    }
                }
                lua_setfield(L, -2, "data");

                lua_pushstring(L, "metatable access denied");
                lua_setfield(L, -2, "__metatable");
            }
            lua_setmetatable(L, -2);
        }

        // register a persistent index for this object
        {
            lua_getglobal(L, ClassName());
            lua_getfield(L, -1, "objects");
            lua_pushvalue(L, -3);
            instance->setScriptReference(luaL_ref(L, -2));
            lua_pop(L, 2);

            XLuaEngine* engine = Implementation::engine(L);
            engine->myInstances[instance->nativePointer()] = instance;
        }

        return 1;
    }

    static int get(lua_State* L)
    {
        XBaseInstance* instance = ExtractPointerHelper<XBaseInstance*>("instance", L, lua_upvalueindex(1));
        if(!instance)
            return luaX_printerror(L, "object is null");

        std::string key = lua_tostring(L, 2);
        if(instance && instance->meta().propertyExists(key))
        {
            push(L, instance->meta().property(key).getVariant(instance->nativePointer()));
        }
        else
        {
            lua_getfield(L, lua_upvalueindex(1), "data");
            lua_getfield(L, -1, key.c_str());
            lua_remove(L, -2);
        }

        return 1;
    }

    static int set(lua_State* L)
    {
        XBaseInstance* instance = ExtractPointerHelper<XBaseInstance*>("instance", L, lua_upvalueindex(1));
        if(!instance)
            return luaX_printerror(L, "object is null");;

        std::string key = lua_tostring(L, 2);

        if(instance->meta().propertyExists(key))
        {
            XBaseProperty& property = instance->meta().property(key);
            property.setVariant(instance->nativePointer(), to(L, 3, property.type()));

            lua_getfield(L, lua_upvalueindex(1), "data");
            lua_pushvalue(L, 3);
            lua_setfield(L, -2, key.c_str());
            lua_pop(L, 1);
        }
        else
        {
            lua_getfield(L, lua_upvalueindex(1), "data");
            lua_pushvalue(L, 3);
            lua_setfield(L, -2, key.c_str());
            lua_pop(L, 1);
        }

        return 0;
    }

    static void push(lua_State* L, const XVariant& variant)
    {
        switch(variant.type())
        {
        case XType::VOID:
            break;
        case XType::OBJECT:
        {
            XBaseInstance* instance = nullptr;
            void* ptr = variant.value<void*>();
            if(!ptr)
                return lua_pushnil(L);

            XLuaEngine* engine = Implementation::engine(L);
            auto iterator = engine->myInstances.find(ptr);
            if(engine->myInstances.end() != iterator)
                instance = iterator->second;

            if(!instance)
            {
                pushNative(L, ptr);
            }
            else
            {
                lua_getglobal(L, ClassName());
                lua_getfield(L, -1, "objects");
                lua_rawgeti(L, -1, instance->scriptReference<int>());
                lua_remove(L, -2);
                lua_remove(L, -2);
            }
            return;
        }
        case XType::BOOL:
        {
            lua_pushboolean(L, variant.value<bool>());
            return;
        }
        case XType::INT:
        {
            lua_pushinteger(L, variant.value<int>());
            return;
        }
        case XType::FLOAT:
        {
            lua_pushnumber(L, variant.value<float>());
            return;
        }
        case XType::DOUBLE:
        {
            lua_pushnumber(L, variant.value<double>());
            return;
        }
        case XType::STRING:
        {
            lua_pushstring(L, variant.value<std::string>().c_str());
            return;
        }
        case XType::VECTOR:
        {
            lua_pushnil(L);
            luaX_printwarning(L, "XLuaEngine::push(...) - Type 'Vector not pushable yet");
            return;
        }
        case XType::LIST:
        {
            lua_pushnil(L);
            luaX_printwarning(L, "XLuaEngine::push(...) - Type 'List not pushable yet");
            return;
        }
        case XType::MAP:
        {
            lua_pushnil(L);
            luaX_printwarning(L, "XLuaEngine::push(...) - Type 'Map not pushable yet");
            return;
        }
        }

        lua_pushnil(L);
        luaX_printerror(L, "XLuaEngine::push(...) - Undefined Type");
    }

    static XVariant to(lua_State* L, int idx, XType type)
    {
        if(lua_isnil(L, idx))
        {
            luaX_printerror(L, "cannot convert nil value");
            return XVariant();
        }
        else if(lua_isuserdata(L, idx))
        {
            if(XType::STRING == type)
            {
                std::stringstream stream;
                stream << lua_touserdata(L, idx);
                return XVariant(stream.str());
            }
        }
        else if(lua_isboolean(L, idx))
        {
            if(XType::STRING == type)
                return XVariant(std::to_string(lua_toboolean(L, idx)));
            else
                return XVariant(lua_toboolean(L, idx));
        }
        else if(lua_isnumber(L, idx))
        {
            if(XType::VOID != type)
            {
                if(XType::FLOAT == type)
                    return XVariant((float) lua_tonumber(L, idx));
                else if(XType::DOUBLE == type)
                    return XVariant((double) lua_tonumber(L, idx));
                else if(XType::STRING == type)
                    return XVariant(std::to_string((double) lua_tonumber(L, idx)));
                else
                    return XVariant((int) lua_tointeger(L, idx));
            }
            else
            {
                if(lua_isinteger(L, idx))
                    return XVariant((int) lua_tointeger(L, idx));
                else
                    return XVariant((double) lua_tonumber(L, idx));
            }
        }
        else if(lua_isstring(L, idx))
            return XVariant(std::string(lua_tostring(L, idx)));

        if(lua_istable(L, idx))
        {
            if(XType::OBJECT == type)
            {
                lua_getmetatable(L, -1);
                {
                    XBaseInstance* instance = ExtractPointerHelper<XBaseInstance*>("instance", L);
                    if(instance)
                        return XVariant(instance->nativePointer(), &instance->meta());

                    void* ptr = ExtractPointerHelper<void*>("ptr", L);
                    if(ptr)
                        return XVariant(ptr, nullptr);
                }
                lua_pop(L, 1);
            }
            else
            {
                luaX_printwarning(L, "XLuaEngine::to(...) - Type 'Table' not convertible yet");
            }
        }

        return XVariant();
    }

    static int pushNative(lua_State* L, void* ptr)
    {
        if(ptr)
        {
            // proxy
            lua_newtable(L);
            {
                // meta
                lua_newtable(L);
                {
                    lua_pushlightuserdata(L, ptr);
                    lua_setfield(L, -2, "ptr");

                    lua_pushvalue(L, 1);
                    lua_setfield(L, -2, "class");

                    lua_pushvalue(L, -1);
                    lua_pushcclosure(L, native_tostring, 1);
                    lua_setfield(L, -2, "__tostring");

                    lua_pushstring(L, "metatable access denied");
                    lua_setfield(L, -2, "__metatable");
                }
                lua_setmetatable(L, -2);
            }
        }
        else
        {
            lua_pushnil(L);
        }

        return 1;
    }

    static int call(lua_State* L)
    {
        XBaseInstance* instance = ExtractPointerHelper<XBaseInstance*>("instance", L, lua_upvalueindex(1));
        if(!instance)
            return luaX_printerror(L, "object is null");

        std::string functionName = lua_tostring(L, lua_upvalueindex(2));
        XBaseFunction& function = instance->meta().function(functionName);

        std::vector<XType> parameterTypes = function.parameterTypes();

        std::size_t arity = std::max<std::size_t>(0, static_cast<std::size_t>(lua_gettop(L) - 1));

        if(arity > parameterTypes.size())
            return luaX_printerror(L, "calling member function '" << functionName << "' with " << arity << " arguments, " << std::to_string(parameterTypes.size()) << " needed.");

        for(int i = 0; i != static_cast<int>(arity); ++i)
        {
            const int valueIndex = i + 2;
            if(lua_isnil(L, valueIndex))
                return luaX_printerror(L, "calling member function '" << functionName << "' with a nil parameter");
        }

        std::vector<XVariant> parameters;
        parameters.reserve(arity);

        for(std::size_t i = 0; i != arity; ++i)
        {
            const XType type = i < parameterTypes.size() ? parameterTypes[i] : XType::VOID;
            const int valueIndex = static_cast<int>(i) + 2;

            parameters.push_back(to(L, valueIndex, type));
        }

    //    std::cout << "[INFO] " << "calling member function '" << functionName << "' with " << parameters.size() << " arguments: " << XVariant(parameters) << std::endl;

        XVariant result;

        try
        {
            result = function.call(instance->nativePointer(), parameters);
        }
        catch(std::exception& e)
        {
            return luaX_printerror(L, "exception raised during a call to '" << functionName << "' with error : " << FormatExceptionErrorHelper(e));
        }

        if(!result)
            return 0;

        push(L, result);
        return 1;
    }

    static int instance_tostring(lua_State* L)
    {
        XBaseInstance* instance = ExtractPointerHelper<XBaseInstance*>("instance", L, lua_upvalueindex(1));

        std::stringstream description;
        if(!instance)
        {
            description << instance->meta().name() << "(NULL)";
        }
        else
        {
            description << "LUA(" << instance->meta().name() << ", " << instance->nativePointer() << ")";
        }

        lua_pushstring(L, description.str().c_str());

        return 1;
    }

    static int destroyed_tostring(lua_State* L)
    {
        XBaseMeta* meta = ExtractPointerHelper<XBaseMeta*>("type", L, lua_upvalueindex(1));

        std::stringstream description;
        if(!meta)
        {
            description << "LUA(UNKNOWN, DESTROYED)";
        }
        else
        {
            description << "LUA(" << meta->name() << ", DESTROYED)";
        }

        lua_pushstring(L, description.str().c_str());

        return 1;
    }

    static int native_tostring(lua_State* L)
    {
        void* ptr = ExtractPointerHelper<void*>("ptr", L, lua_upvalueindex(1));

        std::stringstream description;
        description << "NATIVE(" << ptr << ")";

        lua_pushstring(L, description.str().c_str());

        return 1;
    }

private:
    lua_State* L;

};

XLuaEngine::XLuaEngine() : XScriptEngine(),
    pimpl(new Implementation())
{
    registerSingletonClassByName(XLuaEngine::ClassName(), this);
}

XLuaEngine::~XLuaEngine() = default;

void XLuaEngine::addImportPath(const std::string& path)
{
    pimpl->addImportPath(path);
}

void XLuaEngine::require(const std::string& filename)
{
    pimpl->require(filename);
}

void XLuaEngine::registerClassByName(const std::string& name)
{
    pimpl->registerClass(name);
}

void XLuaEngine::registerSingletonClassByName(const std::string& name, void* ptr)
{
    pimpl->registerSingletonClass(name, ptr);
}
