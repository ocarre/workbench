#ifndef XLUAENGINE_H
#define XLUAENGINE_H

#include <core/xscriptengine.h>
#include <core/xproperty.h>
#include <core/xinstance.h>

#include <functional>
#include <map>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <memory>

class XLuaEngine : public XScriptEngine
{
public:
    XLuaEngine();
    ~XLuaEngine();

    using XScriptEngine::registerClass;
    using XScriptEngine::registerSingletonClass;

    void addImportPath(const std::string& path) override;
    void require(const std::string& filename) override;

protected:
    void registerClassByName(const std::string& name) override;
    void registerSingletonClassByName(const std::string& name, void* ptr) override;

    static constexpr const char* ClassName() { return "XLuaEngine"; }

private:
    class Implementation;
    std::unique_ptr<Implementation> pimpl;

};

#endif
