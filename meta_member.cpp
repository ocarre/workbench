#include <iostream>
#include <vector>

class Component
{
public:
    const std::string& name() const { return myName; }

protected:
    Component(const std::string& name) :
        myName(name)
    {

    }

    static std::string Generate()
    {
        static int id = 0;

        return std::to_string(++id);
    }

private:
    std::string myName;

};

class A : public Component
{
public:
    A() : Component("A" + Generate())
    {

    }
};

class B : public Component
{
public:
    B() : Component("B" + Generate())
    {

    }
};

class C : public Component
{
public:
    C() : Component("C" + Generate())
    {

    }
};

using ComponentType = int;

class BaseRegistry
{
protected:
    static ComponentType Generate()
    {
        return Generator++;
    }

private:
    static ComponentType Generator;

};

ComponentType BaseRegistry::Generator = 0;

template<class T>
class Registry : public BaseRegistry
{
public:
    static ComponentType Id()
    {
        static ComponentType id = Generate();

        return id;
    }

};

class Entity
{
public:
    Entity() :
        myComponentsByTypes()
    {
        
    }

    template<class T>
    T* createComponent()
    {
        T* component = new T;

        const ComponentType typeId = Registry<T>::Id();

        if(myComponentsByTypes.size() < typeId + 1)
            myComponentsByTypes.resize(typeId + 1);

        myComponentsByTypes[typeId].push_back(component);

        return component;
    }

    template<class T>
    std::vector<T*> getComponents()
    {
        const ComponentType typeId = Registry<T>::Id();

        if(typeId < myComponentsByTypes.size())
        {
            return *reinterpret_cast<std::vector<T*>*>(&myComponentsByTypes[typeId]); // is it safe ?
        }

        return {};
    }

private:
    std::vector<std::vector<Component*>> myComponentsByTypes;

};

int main(int argc, char* argv[])
{
    Entity root;

    root.createComponent<A>();
    root.createComponent<B>();
    root.createComponent<A>();
    root.createComponent<C>();
    root.createComponent<B>();
    root.createComponent<A>();

    std::vector<A*> as = root.getComponents<A>();
    std::vector<B*> bs = root.getComponents<B>();
    std::vector<C*> cs = root.getComponents<C>();

    std::cout << "Element A:" << std::endl;
    std::for_each(as.begin(), as.end(), [](auto element) { std::cout << "> " << element->name() << std::endl; });

    std::cout << "Element B:" << std::endl;
    std::for_each(bs.begin(), bs.end(), [](auto element) { std::cout << "> " << element->name() << std::endl; });

    std::cout << "Element C:" << std::endl;
    std::for_each(cs.begin(), cs.end(), [](auto element) { std::cout << "> " << element->name() << std::endl; });

    return 0;
}
