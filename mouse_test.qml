import QtQml
import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15

import QtTest 1.0

TestCase {
    id: root
    width: 200
    height: width
    visible: true

    function test_mouse() {
        console.log("a");
        mouseClick(area);
        console.log("b");
        mousePress(area);
        console.log("c");
        mouseRelease(area);
        console.log("d");
    }

    property int clickCount: 0
    onClickCountChanged: console.log("clickCount", clickCount);

    property int pressCount: 0
    onPressCountChanged: console.log("pressCount", pressCount);
    property int clickCount: 0
    onClickCountChanged: console.log("clickCount", clickCount);

    MouseArea {
        id: area

        onMouseClicked: ++root.clickCount;
        onMousePressed: ++root.pressCount;
        onMouseReleased: ++root.releaseCount;
    }
}
