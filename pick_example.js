function select(...) {
    pickControlPointOnMesh(fetchControlPointData, fetchMeshData).then((v) => { /* do whatever you want with selected cp / mesh contained in v */ });
}

function fetchControlPointData(pickingReply) {
    // get and return relevant control point data from picking reply
}

function fetchMeshData(pickingReply) {
    // get and return relevant mesh data from picking reply
}

// ---

function pickControlPointOnMesh(controlPointDataFetcher, meshDataFetcher) {
    var controlPointPickingPromise = pickAndWrapToPromise(controlPointDataFetcher);
    var meshPickingPromise = pickAndWrapToPromise(meshDataFetcher);

    return Promise.all([controlPointPickingPromise, meshPickingPromise]).then((v) => { return { cp: v[0], mesh: v[1] }; });
}

function pickAndWrapToPromise(fetcher) {
    return new Promise((resolve, reject) => {
        var reply = meshPicker.pick(...);
        reply.completed.connect((r) => { // on fetch les données au bon moment, on wrap pas la reply dans une promise mais le fait que les données ont été récupérées
            if(!r.isValid()) {
                reject();
                return;
            }

            var data = fetcher(r);
            resolve(data);
        });
    });
}
