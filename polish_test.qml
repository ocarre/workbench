import QtQml
import QtQuick 2.14
import QtQuick.Window 2.12
import QtTest 1.0

Column {
    id: root
    height: 400

    Component.onCompleted: console.log("column", root.height)

    readonly property var rectHeight: 200

    TestCase {
        name: "polish"
        when: windowShown

        function test_polish() {
            let iterations = 10;
            for(let i = 0; i < iterations; ++i) {
                let height = root.height;

                let count = 1 + ((i + 1) % 2);

                repeater.model = count;

                // waitForRendering(root);

                // compare(root.height, rectHeight * count, `${root.height} vs ${rectHeight * count}, with count == ${count}`);

                console.log("<", i);
                console.log(">", i);


                console.log("a");

                // waitForItemPolished(root);

                console.log("b");

                // root.forceLayout();
                // if(0 !== i)
                //     waitForItemPolished(root);

                console.log("c", root.height);

                if(isPolishScheduled(root));
                    waitForItemPolished(root);

                console.log("d", root.height);

                // var w0 = waitForRendering(root, 1);
                // console.log("e", w0);
                // var w1 = waitForRendering(root, Number.MAX_SAFE_INTEGER);
                waitForRendering(root, 1);
                console.log("e", root.height);
                var w1 = waitForRendering(root, Number.MAX_SAFE_INTEGER);
                console.log("f", w1);

                var image = grabImage(root);
                image.save("/Users/ocarre/Desktop/test/test_" + i + ".png");

                console.log("g", root.height, image.height);

                // compare(root.height, image.height);

                console.log("h");

                // sleep(200);
            }
        }
    }

    Repeater {
        id: repeater

        model: 0
        delegate: Rectangle {
            width: 200
            height: rectHeight

            color: ["red", "green", "blue"][index]
        }
    }
}
