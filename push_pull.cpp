#include <iostream>
#include <vector>
#include <type_traits>

class Node
{
protected:
    Node(const std::string& name = {}) :
        myName(name),
        myInputs(),
        myOutputs(),
        myIsDirty(false),
        myIsLooping(false)
    {
        
    }

    const std::string& name() const { return myName; }

    struct LoopLock
    {
        LoopLock(bool* loop) :
            myIsLooping(loop)
        {
            if(*myIsLooping)
                throw std::runtime_error("Engine loop detected");

            *myIsLooping = true;
        }

        ~LoopLock()
        {
            *myIsLooping = false;
        }

    private:
        bool* myIsLooping;

    };

    virtual void update()
    {
        if(!isDirty())
            return;

        std::cout << "Requesting node update: " << (myName.empty() ? "*Unnamed*" : "'" + myName + "'" ) << std::endl;

        LoopLock loopLock(&myIsLooping);
        (void) loopLock;

        std::for_each(myInputs.begin(), myInputs.end(), [](auto input) { input->update(); });

        compute();

        updated();
    }

    void updated()
    {
        if(!myIsDirty)
            return;

        std::cout << "Node updated: " << (myName.empty() ? "*Unnamed*" : "'" + myName + "'" ) << std::endl;

        propagateDirty();
        unmarkDirty();
    }

    virtual void compute() {}

    bool isDirty() const { return myIsDirty; }
    void markDirty() { setDirty(true); }
    void unmarkDirty() { setDirty(false); }

    int inputCount() const { return myInputs.size(); }
    int outputCount() const { return myOutputs.size(); }

    void addDependency(Node* parent)
    {
        parent->addOutput(this);
        addInput(parent);

        markDirty();
    }
    
private:
    void addInput(Node* input)
    {
        myInputs.push_back(input);
    }

    void addOutput(Node* output)
    {
        myOutputs.push_back(output);
    }

    void setDirty(bool dirty)
    {
        if(dirty == myIsDirty)
            return;

        myIsDirty = dirty;

        if(myIsDirty)
            std::cout << "Node dirty: " << (myName.empty() ? "*Unnamed*" : "'" + myName + "'" ) << std::endl;
        else
            std::cout << "Node clean: " << (myName.empty() ? "*Unnamed*" : "'" + myName + "'" ) << std::endl;

        if(myIsDirty)
            propagateDirty();
    }

    void propagateDirty()
    {
        std::for_each(myOutputs.begin(), myOutputs.end(), [](auto child) { child->markDirty(); });
    }

private:
    std::string myName;
    std::vector<Node*> myInputs;
    std::vector<Node*> myOutputs;
    bool myIsDirty;
    bool myIsLooping;

};

template<class T>
class Property;

class BaseProperty : public Node
{
    friend class Engine;

protected:
    BaseProperty(const std::string& name) : Node(name)
    {
        
    }

};

template<class T>
class Property : public BaseProperty
{
public:
    Property(const T& value = {}, const std::string& name = {}) : BaseProperty(name),
        myValue(std::make_shared<T>(value))
    {
        std::cout << "Constructing property: " << (name.empty() ? "*Unnamed*" : "'" + name + "'" ) << std::endl;

        updated();
    }

    T get()
    {
        std::cout << "Getting property: " << (name().empty() ? "*Unnamed*" : "'" + name() + "'" ) << std::endl;

        update();

        return *myValue;
    }

    void set(const T& value)
    {
        std::cout << "Setting property: " << (name().empty() ? "*Unnamed*" : "'" + name() + "'" ) << std::endl;

        *myValue = value;

        updated();
    }

protected:

private:
    std::shared_ptr<T> myValue;

};

class Engine : public Node
{
public:
    Engine(const std::string& name) : Node(name)
    {
        std::cout << "Constructing engine: " << (name.empty() ? "*Unnamed*" : "'" + name + "'" ) << std::endl;
    }

protected:
    template<class I>
    void addInput(I input)
    {
        addDependency(input);
    }

    template<class O>
    void addOutput(O output)
    {
        if(0 != output->inputCount())
            throw std::runtime_error("Cannot add more than one input to a property");

        output->addDependency(this);
    }
    
};

template<class F, class O, class ... I>
class FunctorEngine : public Engine
{
    template <typename> struct is_tuple: std::false_type {};
    template <typename ...T> struct is_tuple<std::tuple<T...>>: std::true_type {};

    template<class Result, class Os>
    struct extractor
    {
        void operator()(Os outputs, Result result)
        {
            outputs->set(result);
        }
    };

    template<class ... Result, class ... Os>
    struct extractor<std::tuple<Result...>, std::tuple<Os...>>
    {
        void operator()(std::tuple<Os...> outputs, std::tuple<Result...> result)
        {
            transform(outputs, result, [](auto a, auto b) { a->set(b); });
        }

        template<typename... Ts, typename... Us, typename Func>
        void transform(std::tuple<Ts...> const& t, std::tuple<Us...> const& u, Func f)
        {
            static_assert(sizeof...(Ts) == sizeof...(Us), "FunctorEngine does not have the same number of outputs as its functor");
            transformImpl(t, u, f, std::make_integer_sequence<int, sizeof...(Ts)>());
        }

        template<typename T, typename U, typename Func, int... Is>
        void transformImpl(T&& t, U&& u, Func f, std::integer_sequence<int, Is...>)
        {
            (f(std::get<Is>(t), std::get<Is>(u)), ...);
        }
    };

public:
    FunctorEngine(const I&... inputs, F functor, O& outputs, const std::string& name) : Engine(name),
        myFunctor(functor),
        myInputs(inputs...),
        myOutputs(outputs)
    {
        if constexpr (is_tuple<O>::value)
            std::apply([this](auto ... output) { (addOutput(output), ...); }, myOutputs);
        else
            std::apply([this](auto ... output) { (addOutput(output), ...); }, std::forward_as_tuple(myOutputs));

        std::apply([this](auto ... input) { (addInput(input), ...); }, myInputs);
    }

private:
    void compute() override
    {
        std::apply(&FunctorEngine::call<I...>, std::tuple_cat(std::make_tuple(this), myInputs));

        // at the end of the computation, engine updated its output itself so its considered undirty
        unmarkDirty();
    }

    template<class ... Is>
    auto call(Is... inputs)
    {
        std::cout << (name().empty() ? "*Unnamed*" : "'" + name() + "'" ) << " engine computation in progress... " << std::endl;

        auto tuple = myFunctor((inputs->get())...);

        extractor<decltype(tuple), O>()(myOutputs, tuple);
        
        std::cout << (name().empty() ? "*Unnamed*" : "'" + name() + "'" ) << " engine computation done! " << std::endl;
    }

private:
    F myFunctor;
    std::tuple<I...> myInputs;
    O myOutputs;

};

template<class O, class F, class ... I>
FunctorEngine<F, O, I...> FunctorEngine_Create(const std::string& name, O outputs, F functor, I... inputs)
{
    return FunctorEngine<F, O, I...>(inputs..., functor, outputs, name);
}

#include <cstddef>

int main(int argc, char* argv[]) try
{
    using t = ::nullptr_t;
    
    std::cout << "Start" << std::endl;

    Property<int> a(5, "a");
    Property<int> b(2, "b");

    Property<int> c({}, "c");
    Property<int> d({}, "d");
    Property<int> e({}, "e");
    
    auto sum = FunctorEngine_Create("Sum_0", &c, std::plus<int>(), &a, &b);
    auto sumAndMul = FunctorEngine_Create("Sum_1", std::make_tuple(&d, &e), [](int x, int y) { return std::make_tuple(std::plus<int>()(x, y), x * y); }, &a, &c);

    const int e_result = e.get();
    const int d_result = d.get();
    const int c_result = c.get();
    const int b_result = b.get();
    const int a_result = a.get();

    std::cout << "Sum - a: " << a_result << ", b: " << b_result << ", c: " << c_result << ", d: " << d_result << ", e: " << e_result << std::endl;

    std::cout << "End" << std::endl;

    return 0;
}
catch(std::exception& e)
{
    std::cerr << "Exception caught: " << e.what() << std::endl;
    return 1;
}
catch(...)
{
    std::cerr << "Unknown exception caught" << std::endl;
    return 1;
}
