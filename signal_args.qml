import QtQml
import QtQuick 2.15
import QtQml 2.15

QtObject {
    signal test(int a, string b)
    onTest: (a, b) => console.log(a, b)

    Component.onCompleted: {
        // test(3)
        test(3, "hello")
        test(3, "hello", 7.62)
    }
}
