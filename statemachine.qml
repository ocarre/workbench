import QtQuick
import QtQuick.Controls
import QtQuick.Window
import QtQuick.Controls.Material
import QtQml.Models
import QtQml.StateMachine

ApplicationWindow {
    id: root

    PluginStateMachine {
        id: machine
    }

    PluginMiddleware {
        id: middleware

        machine: machine
    }

    PluginStore {
        id: store

        machine: machine
    }

    PluginView {
        anchors.fill: root

        store: store
    }
}

// PluginView.qml
// example featuring only a state button panel and a busy indicator
// store should expose everything needed to be used from here, we should not need to pass the store further on

Item {
    id: root
    required property var store

    StateButtonPanel {
        anchors.fill: parent
        states: root.store.states
    }

    BusyIndicator {
        anchors.fill: parent
        visible: root.store.busy
    }
}

// StateButtonPanel.qml

Row {
    property alias states: repeater.model

    Repeater {
        id: repeater
        delegate: Button {
            readonly property var state: modelData
            
            enabled: state.reachable // property updated according to the current state
            text: state.name

            onClicked: state.gotoAction.dispatch();
        }
    }
}

// PluginStateMachine.qml

StateMachine {
    id: machine
    initialState: stateA
    running: true

    property var currentState

    State {
        id: stateA
        objectName: "stateA"

        readonly property var storeData: QtObject {
            property int param0
            property int param1
            property int param2

            readonly property var gotoBAction: gotoB

            function isReachable() {
                return machine.currentState.canReach(stateA);
            }
        }

        ActionCreatorRegister {
            ActionCreator {
                id: gotoB
            }
        }

        // onEntered: () => console.log("OnEntered", objectName)
        // onExited: () => console.log("OnExited", objectName)

        readonly property var transitionB: transition_A_B
        readonly property var transitionC: transition_A_C

        SignalTransition {
            id: transition_A_B
            targetState: stateB
            signal: next
            guard: store.param0 && store.param1
            // evaluator: () => Promise.resolve(true);

            signal next()
        }

        SignalTransition {
            id: transition_A_C
            targetState: stateC
            signal: next
            guard: store.param2
            // evaluator: () => Promise.resolve(true);

            signal next()
        }
    }

    State {
        id: stateB
        objectName: "stateB"

        onEntered: () => console.log("OnEntered", objectName);
        onExited: () => console.log("OnExited", objectName);
    }

    State {
        id: stateC
        objectName: "stateC"

        onEntered: () => console.log("OnEntered", objectName);
        onExited: () => console.log("OnExited", objectName);
    }
}

// PluginStore.qml

Store {
    id: store

    required property var machine

    property bool busy: false

    readonly property var states: machine.states.map(x => x.storeData)
}

// StateMachineSubStore.qml

// ObjectGroup
// {
//     required property var machine // machine

//     property var statesData: stateDataInstantiator.objects

//     Instantiator {
//         id: stateDataInstantiator
//         model: machine.states.filter(x => x.storeData);
//         delegate: QtObject {
//             readonly property alias data: dataInstantiator.object
//             readonly property bool reachable: data.isReachable()
//             readonly property bool gotoAction: data.gotoAction

//             Instantiator {
//                 id: dataInstantiator
//                 delegate: modelData
//             }
//         }

//         property var objects: []
//         onObjectAdded: (index, object) => objects.push(object);
//         onObjectRemoved: (index, object) => objects.splice(objects.indexOf(object), 1);
//     }
// }
