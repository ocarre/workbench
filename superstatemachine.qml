import QtQuick
import QtQuick.Controls
import QtQuick.Window
import QtQml.StateMachine
import QtQuick.Controls.Material

Row {
    id: root

    QtObject {
        id: store

        readonly property var data: ({ [stateA.objectName]: stateAData, [stateB.objectName]: stateBData, [stateC.objectName]: stateCData })

        readonly property var stateAData: QtObject {
            readonly property color color: "red"
        }

        readonly property var stateBData: QtObject {
            readonly property color color: "green"
        }

        readonly property var stateCData: QtObject {
            readonly property color color: "blue"
        }

        property bool param0: true
        property bool param1: true
        property bool param2: true
    }

    StateMachine {
        id: machine
        initialState: stateA
        running: true

        property var currentState: null

        readonly property var stateA: stateA
        readonly property var stateB: stateB
        readonly property var stateC: stateC

        SuperState {
            id: stateA
            objectName: "stateA"

            // onEntered: () => console.log("OnEntered", objectName)
            // onExited: () => console.log("OnExited", objectName)

            readonly property var transitionB: transition_A_B
            readonly property var transitionC: transition_A_C

            SignalTransition {
                id: transition_A_B
                targetState: stateB
                signal: next
                guard: store.param0 && store.param1
                // evaluator: () => Promise.resolve(true);

                signal next()
            }

            SignalTransition {
                id: transition_A_C
                targetState: stateC
                signal: next
                guard: store.param2
                // evaluator: () => Promise.resolve(true);

                signal next()
            }
        }

        SuperState {
            id: stateB
            objectName: "stateB"

            onEntered: () => console.log("OnEntered", objectName);
            onExited: () => console.log("OnExited", objectName);
        }

        SuperState {
            id: stateC
            objectName: "stateC"

            onEntered: () => console.log("OnEntered", objectName);
            onExited: () => console.log("OnExited", objectName);
        }

        function evaluate(from, to) {
            var filteredTransitions = from.transitions.filter(x => to === x.target);
            return filteredTransitions.every(x => x.condition);
        }

        function move(from, to) {
            if(!evaluate(from, to)) {
                console.error("Evaluate must be checked before calling move");
                return;
            }

            evaluating = true;

            var promises = []

            // foreach(let transition in from.transitions[to])
            //     promises.push(Promise.resolve(transition.evaluator()));
            
            // return Promise.race(promises).then(doMove(from, to));
        }
    }

    Button {
        enabled: false
        text: stateA.objectName
    }

    Button {
        enabled: stateA.active && stateA.transitionB.guard
        text: stateB.objectName

        onClicked: stateA.transitionB.signal()
    }
    
    Button {
        enabled: stateA.active && stateA.transitionC.guard
        text: stateC.objectName

        onClicked: stateA.transitionC.signal()
    }

    // StateSelector {
    //     // enabled: !machine.state.evaluating
    //     states: machine.states

    //     delegate: Button { // StateButton
    //         readonly property var state: modelData
    //         enabled: machine.currentState != state && state.evaluate(machine.currentState)

    //         // onClicked: (machine.currentState, state) => machine.move(machine.currentState, state);

    //         // # implicit
    //         // enabled: state.enabled
    //         // text: state.name
    //     }
    // }
}
