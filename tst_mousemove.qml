import QtQuick
import QtTest

MouseArea {
    id: root
    anchors.fill: parent

    hoverEnabled: true

    property int moveCount: 0

    onPositionChanged: ++moveCount;

    TestCase {
        id: testcase
        when: windowShown

        function test_mousemove() {
            var expectedMoveCount = 4;
            for(var i = 0; i < expectedMoveCount; ++i)
                mouseMove(root, i, i * 2);

            compare(moveCount, expectedMoveCount); // => fail! actual : 3, expected: 4
        }
    }
}
