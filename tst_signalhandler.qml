import QtQuick

Item {
    id: testcase
    
    property BadSignalHandler pickingOperations: BadSignalHandler {
        onPickingReplyReceived: (replyData) => {
            
        }
        onDoublePickingReplyReceived: (firstReplyData, secondReplyData) {
            
        }
    }
}
