const socket = new WebSocket('ws://localhost:8080');

socket.addEventListener('open', (event) => {
  console.log('connected to server');
});

socket.addEventListener('message', (event) => {
  const data = JSON.parse(event.data);
  
  if (data.asset) { // Si la donnée est une géométrie en format glTF
    const loader = new THREE.GLTFLoader();
    loader.parse(data, '', (gltf) => {
      const mesh = gltf.scene.children[0];
      scene.add(mesh); // Ajout du mesh à la scène
    });
  } else if (data.x !== undefined && data.y !== undefined && data.z !== undefined) { // Si la donnée est une rotation
    console.log(cube.rotation);
    cube.rotation.x += data.x;
    cube.rotation.y += data.y;
    cube.rotation.z += data.z;
  }
});

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 100);

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshPhongMaterial({ color: 0x00ff00 });
const cube = new THREE.Mesh(geometry, material);
scene.add(cube);

camera.position.z = 5;

const light = new THREE.DirectionalLight(0xffffff);
light.position.set(1, 1, 1);
scene.add(light);

function animate() {
  requestAnimationFrame(animate);

  renderer.render(scene, camera);
}

animate();
