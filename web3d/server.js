import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import GLTFExporter from 'three-gltf-exporter';
import { createServer } from 'http';
import WebSocket, { WebSocketServer } from 'ws';
import { fileURLToPath } from 'url';
import path from "path";
import fs from "fs";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Create a WebSocket server
const server = createServer();
const wss = new WebSocketServer({ server });

// Send the GLTF data to clients upon connection
server.on('connection', (socket) => {
    console.log("Connexion établie");
});

const vertices = [[1, 0, 0], [0, 1, 0], [-1, 0, 0]];

const vertexCount = 3;
const vectorSize = 3;

const noise = 0.01;

var data = new Float32Array(vectorSize * vertexCount);
for(var i = 0; i < vertexCount; ++i) {
    for(var j = 0; j < vectorSize; ++j) {
        const index = i * vectorSize + j;
        data[index] = vertices[i][j];
    }
}

function update() {
    for(var i = 0; i < vertexCount; ++i) {
        for(var j = 0; j < vectorSize; ++j) {
            const jitter = (Math.random() * 2 - 1) * noise;

            const index = i * vectorSize + j;
            data[index] = data[index] + jitter;
        }
    }
    
    wss.clients.forEach(client => {
        client.send(JSON.stringify(data));
    });
}

// Periodically update the vertex positions of the cube
const period = 16; // milliseconds
setInterval(() => {    
    update();
}, period);

server.listen(3000, () => {
    console.log('Serveur démarré sur le port 3000');
});
